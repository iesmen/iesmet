/**
 * This file is part of IESmet.
 * <p/>
 * Foobar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * IESmet is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.ies.iesmet.serial.interpretation;

/**
 * This interface defines an Interpreter.
 *
 * <p>It converts sensor raw values to a human readable one.</p>
 *
 * @author Jorge Torregrosa {@literal <jtorregrosalloret@gmail.com>}
 * @version 1.0
 */
public interface Interpreter {

    /**
     * Interprets raw sensor value to a human readable type.
     *
     * @param value value to be interpreted.
     * @return already interpreted value.
     */
    double interpret(final float value);

    /**
     * Gets interpreted value unit.
     *
     * @return interpreted value unit.
     */
    String getUnit();
}
