package es.ies.iesmet.serial.event;

import es.ies.iesmet.event.CallbackEvent;
import es.ies.iesmet.event.StatusResponse;

import java.util.function.Consumer;

/**
 * Created by jtorregrosa on 24/04/15.
 */
public class CloseConnectionEvent extends CallbackEvent<StatusResponse<Void>> {

    public CloseConnectionEvent(Consumer<StatusResponse<Void>> callback) {
        super(callback);
    }
}
