package es.ies.iesmet.serial.event;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.SubscriberExceptionContext;
import com.google.common.eventbus.SubscriberExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by jtorregrosa on 23/03/15.
 */
public class EventBusWrapper {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventBusWrapper.class);

    private EventBus eventBus;

    private String busName;

    public EventBusWrapper() {
        this("Undefined");
    }

    public EventBusWrapper(final String identifier) {
        this.busName = identifier;
        this.eventBus = new EventBus(new ExceptionHandler());
    }

    public void post(final Object event) {
        eventBus.post(event);
    }

    public void register(final Object subscriber) {
        eventBus.register(subscriber);
    }

    public void unregister(final Object subscriber) {
        eventBus.unregister(subscriber);
    }

    private class ExceptionHandler implements SubscriberExceptionHandler {
        @Override
        public void handleException(final Throwable ex, final SubscriberExceptionContext context) {
            LOGGER.error("There is a problem in bus '" + busName +
                    "'. Problem is related to event '" + context.getEvent().getClass().getCanonicalName() +
                    "' and subscriber '" + context.getSubscriber().getClass().getCanonicalName() +
                    "' with method '" + context.getSubscriberMethod().getName() + "'.", ex);
        }
    }
}
