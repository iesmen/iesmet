package es.ies.iesmet.serial.event;

import es.ies.iesmet.event.StatusResponse;
import es.ies.iesmet.event.TimedCallbackEvent;
import es.ies.iesmet.serial.entity.Handshake;

import java.util.function.Consumer;

/**
 * Created by jtorregrosa on 24/04/15.
 */
public class HandshakeRequestCallbackEvent extends TimedCallbackEvent<StatusResponse<Handshake>> {


    public HandshakeRequestCallbackEvent(final long timeoutMillis, final Consumer<StatusResponse<Handshake>> callback) {
        super(timeoutMillis, callback);
    }
}
