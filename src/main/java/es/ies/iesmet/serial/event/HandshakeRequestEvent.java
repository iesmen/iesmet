package es.ies.iesmet.serial.event;

import es.ies.iesmet.event.FutureEvent;
import es.ies.iesmet.event.StatusResponse;
import es.ies.iesmet.serial.entity.Handshake;

/**
 * Created by jtorregrosa on 24/04/15.
 */
public class HandshakeRequestEvent extends FutureEvent<StatusResponse<Handshake>> {


}
