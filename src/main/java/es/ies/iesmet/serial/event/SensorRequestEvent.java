package es.ies.iesmet.serial.event;

import es.ies.iesmet.event.Event;
import es.ies.iesmet.serial.entity.Request;

/**
 * Created by jtorregrosa on 24/04/15.
 */
public class SensorRequestEvent extends Event<Request> {

    public SensorRequestEvent(Request response) {
        super(response);
    }
}
