package es.ies.iesmet.serial.event;

import es.ies.iesmet.event.Event;
import es.ies.iesmet.serial.entity.SerialPacket;

/**
 * Created by jtorregrosa on 24/04/15.
 */
public class SerialPacketEvent extends Event<SerialPacket> {

    public SerialPacketEvent(SerialPacket response) {
        super(response);
    }
}
