package es.ies.iesmet.serial.event;

import es.ies.iesmet.event.CallbackEvent;
import es.ies.iesmet.event.StatusResponse;

import java.util.Collection;
import java.util.function.Consumer;

/**
 * Created by jtorregrosa on 24/04/15.
 */
public class GetAvailablePortsEvent extends CallbackEvent<StatusResponse<Collection<String>>> {

    public GetAvailablePortsEvent(Consumer<StatusResponse<Collection<String>>> callback) {
        super(callback);
    }
}
