package es.ies.iesmet.serial.event;

import es.ies.iesmet.event.CallbackEvent;
import es.ies.iesmet.event.StatusResponse;
import es.ies.iesmet.event.TimedCallbackEvent;

import java.util.function.Consumer;

/**
 * Created by jtorregrosa on 24/04/15.
 */
public class EstablishConnectionEvent extends TimedCallbackEvent<StatusResponse<Void>> {
    private final String portName;
    private final int baudRate;

    public EstablishConnectionEvent(final String portName, final int baudRate, final long timeoutMillis, Consumer<StatusResponse<Void>> callback) {
        super(timeoutMillis, callback);
        this.portName = portName;
        this.baudRate = baudRate;
    }

    public String getPortName() {
        return portName;
    }

    public int getBaudRate() {
        return baudRate;
    }
}
