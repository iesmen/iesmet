package es.ies.iesmet.serial.event;

import es.ies.iesmet.event.Event;
import es.ies.iesmet.serial.entity.Handshake;

/**
 * Created by jtorregrosa on 24/04/15.
 */
public class HandshakeEvent extends Event<Handshake> {

    public HandshakeEvent(Handshake response) {
        super(response);
    }
}
