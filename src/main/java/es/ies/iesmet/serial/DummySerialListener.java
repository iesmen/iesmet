/**
 * This file is part of IESmet.
 * <p/>
 * Foobar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * IESmet is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.ies.iesmet.serial;

import es.ies.iesmet.serial.entity.Handshake;
import es.ies.iesmet.serial.entity.SerialPacket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This implementation of SerialListener do nothing but registering the event to log.
 * It is useful for development purposes.
 *
 * @author Jorge Torregrosa {@literal <jtorregrosalloret@gmail.com>}
 * @version 1.0
 */
public class DummySerialListener implements SerialListener {
    /**
     * Internal class logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(DummySerialListener.class);

    @Override
    public void onSensorAvailable(final SerialPacket packet) {
        LOGGER.info(packet.toString());
    }

    @Override
    public void onHandshakeAvailable(final Handshake handshake) {
        LOGGER.info(handshake.toString());
    }
}
