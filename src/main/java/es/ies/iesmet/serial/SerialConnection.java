/**
 * This file is part of IESmet.
 * <p/>
 * Foobar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * IESmet is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.ies.iesmet.serial;

import es.ies.iesmet.event.StatusResponse;
import es.ies.iesmet.serial.entity.*;
import es.ies.iesmet.serial.event.*;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import org.apache.logging.log4j.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MarkerFactory;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.Date;

/**
 * @author Jorge Torregrosa {@literal <jtorregrosalloret@gmail.com>}
 * @version 1.0
 */
public class SerialConnection implements AutoCloseable, SerialPortEventListener {

    /**
     * Internal class logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(SerialConnection.class);

    /**
     * Associated serial port name.
     */
    private final SerialPort port;

    /**
     * Registered listeners.
     */
    //private final Collection<SerialListener> listeners;

    /**
     * Main sensor factory.
     */
    private final SensorFactory sensorFactory;

    /**
     * Main request factory.
     */
    private final RequestFactory requestFactory;

    /**
     * Bytes left until the end of communication packet.
     */
    private int bytesLeft;

    /**
     * Internal status.
     */
    private InternalStatus status;

    /**
     * Port Baud Rate.
     */
    private int baudRate;

    private EventBusWrapper serialBus;

    private HandshakeRequestCallbackEvent hRequest;

    /**
     * Constructs a serial connection object providing a port name, a sensor factory and a request factory.
     *
     * @param port           serial port name.
     * @param sensorFactory  sensor factory.
     * @param requestFactory request factory.
     */
    SerialConnection(final SerialPort port, final int baudRate, final SensorFactory sensorFactory, final RequestFactory requestFactory, final EventBusWrapper serialBus) {
        this.port = port;
        //this.listeners = new ArrayList<>();
        this.bytesLeft = 0;
        this.sensorFactory = sensorFactory;
        this.status = InternalStatus.IS_READY;
        this.requestFactory = requestFactory;
        this.baudRate = baudRate;
        this.hRequest = null;
        this.serialBus = serialBus;
    }

    /**
     * Registers a new serial listener for this port.
     *
     * @param listener SerialListener to be registered.
     */
    /*public void addListener(final SerialListener... listener){
        listeners.addAll(Arrays.asList(listener));
    }*/

    /**
     * Sends a request via serial port.
     *
     * @param request request to be sent.
     * @throws SerialException if it cannot performs the communication.
     */
    public void send(final Request request) throws SerialException {
        LOGGER.debug("Sending request '" + request.getType().name() + "' to device.");
        try {
            port.writeBytes(request.getSerialCodes());
        } catch (SerialPortException e) {
            throw new SerialException("Cannot send request.", e);
        }

        switch(request.getType()){
            case RT_KEEP_ALIVE:
                status = InternalStatus.IS_WAITING_FOR_KEEPALIVE;
                break;
            case RT_WAKE_UP:
                status = InternalStatus.IS_WAITING_FOR_WAKE_UP;
                break;
            case RT_SENSOR_REQUEST:
                status = InternalStatus.IS_WAITING_FOR_PACKET;
                break;
            case RT_HANDSHAKE:
                status = InternalStatus.IS_WAITING_FOR_HANDSHAKE;
                break;
            case RT_GENERIC1:
            case RT_GENERIC2:
            case RT_GENERIC3:
            case RT_GENERIC4:
            case RT_GENERIC5:
                status = InternalStatus.IS_WAITING_FOR_ACK;
                break;
        }
    }

    /**
     * Returns associated request factory.
     *
     * @return associated request factory.
     */
    public RequestFactory getRequestFactory(){
        return requestFactory;
    }

    /**
     * Checks if the underlying connection is closed.
     *
     * @return TRUE if is closed, FALSE if it isn't.
     */
    public boolean isClosed() {
        return !port.isOpened();
    }

    /**
     * Gets this connection port name.
     *
     * @return port name.
     */
    public String getPortName() {
        return port.getPortName();
    }

    /**
     * Gets this connection baud rate.
     *
     * @return port baud rate.
     */
    public int getBaudRate() {
        return baudRate;
    }

    @Override
    public void close() throws Exception {
        port.closePort();
        LOGGER.info("Serial port '{}' closed successfully.", port.getPortName());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SerialConnection that = (SerialConnection) o;

        return port.getPortName().equals(that.port.getPortName());
    }

    @Override
    public int hashCode() {
        return port.getPortName().hashCode();
    }

    @Override
    public void serialEvent(final SerialPortEvent event) {
        LOGGER.trace("Serial event type '{}' received from '{}', '{}' bytes.", event.getEventType(), event.getPortName(), event.getEventValue());
        if (event.isRXCHAR()) {

            switch (status) {
                case IS_WAITING_FOR_PACKET:
                    onPacketReception(event);
                    break;
                case IS_WAITING_FOR_HANDSHAKE:
                    onHandshakeReception(event);
                    break;
                case IS_WAITING_FOR_KEEPALIVE:
                    break;
                case IS_WAITING_FOR_WAKE_UP:
                    break;
                case IS_WAITING_FOR_ACK:
                    onAckReception();
                    break;
                case IS_READY:
                    break;
            }

        }
    }

    private void onAckReception(){
        try {
            final ByteBuffer buffer = ByteBuffer.wrap(port.readBytes(1));
            buffer.order(ByteOrder.LITTLE_ENDIAN);
            final int ack = buffer.get();
            LOGGER.trace("Incoming packet length: {} bytes", bytesLeft);
            serialBus.post(new ACKEvent());
            status = InternalStatus.IS_READY;
        } catch (SerialPortException ex) {
            LOGGER.error("There is an error guessing packet length: " + port.getPortName(), ex);
        }
    }

    /**
     * Callback called when a serial sensor packet is received.
     *
     * @param event serial event.
     */
    private void onPacketReception(final SerialPortEvent event) {
        if (event.getEventValue() == bytesLeft) {
            try {
                final SerialPacket packet = readPacket();
                bytesLeft = 0;
                /*for (final SerialListener listener : listeners) {
                    listener.onSensorAvailable(packet);
                }*/
                serialBus.post(new SerialPacketEvent(packet));
            } catch (final SerialPortException | BufferUnderflowException | BufferOverflowException ex) {
                LOGGER.error("There is an error reading data from: " + port.getPortName(), ex);
            } finally {
                status = InternalStatus.IS_READY;
            }
        } else if (event.getEventValue() > 2 && bytesLeft == 0) {
            try {
                final ByteBuffer lengthBuffer = ByteBuffer.wrap(port.readBytes(2));
                lengthBuffer.order(ByteOrder.LITTLE_ENDIAN);
                bytesLeft = (lengthBuffer.getShort() - 2);
                LOGGER.trace("Incoming packet length: {} bytes", bytesLeft);
            } catch (SerialPortException ex) {
                LOGGER.error("There is an error guessing packet length: " + port.getPortName(), ex);
            }
        }
    }

    /**
     * Callback called when a handshake packet is received.
     *
     * @param event serial event.
     */
    private void onHandshakeReception(final SerialPortEvent event) {
        if (event.getEventValue() == bytesLeft) {
            try {
                final Handshake handshake = readHandshake();
                bytesLeft = 0;
                /*for (final SerialListener listener : listeners) {
                    listener.onHandshakeAvailable(handshake);
                }*/
                serialBus.post(new HandshakeEvent(handshake));
                if (hRequest != null) {
                    hRequest.set(new StatusResponse<>(0, "Handshake received succesfully", handshake));
                    hRequest=null;
                }

            } catch (SerialPortException | BufferUnderflowException | BufferOverflowException ex) {
                LOGGER.error("There is an error reading data from: " + port.getPortName(), ex);
            } finally {
                status = InternalStatus.IS_READY;
            }
        } else if (event.getEventValue() > 2 && bytesLeft == 0) {
            try {
                final ByteBuffer lengthBuffer = ByteBuffer.wrap(port.readBytes(2));
                lengthBuffer.order(ByteOrder.LITTLE_ENDIAN);
                bytesLeft = (lengthBuffer.getShort() - 2);
                LOGGER.trace("Incoming packet length: {} bytes", bytesLeft);
            } catch (SerialPortException ex) {
                LOGGER.error("There is an error guessing packet length: " + port.getPortName(), ex);
            }
        }
    }

    /**
     * Reads a handshake packet directly from port.
     *
     * @return a handshake object holding useful communication information.
     * @throws SerialPortException if there is a problem reading data.
     */
    private Handshake readHandshake() throws SerialPortException {
        final ByteBuffer buffer = ByteBuffer.allocateDirect(bytesLeft);
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        buffer.put(port.readBytes(bytesLeft));
        buffer.flip();
        return interpretHandshake(buffer);

    }

    /**
     * Reads a serial sensor packet directly from port.
     *
     * @return a sensor object holding sensor information.
     * @throws SerialPortException if there is a problem reading data.
     */
    private SerialPacket readPacket() throws SerialPortException {
        final ByteBuffer buffer = ByteBuffer.allocateDirect(bytesLeft);
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        buffer.put(port.readBytes(bytesLeft));
        buffer.flip();
        return interpretPacket(buffer);

    }

    /**
     * Interpret bytes of a packet. This method parse raw data into a structured java object.
     *
     * @param buffer raw data buffer.
     * @return interpreted serial packet.
     */
    private SerialPacket interpretPacket(final ByteBuffer buffer) {
        final SerialPacket packet = new SerialPacket();

        final int headerOffset = buffer.get();
        final int availableSensors = buffer.get();
        final int libVersion = buffer.get();


        for (int i = 0; i < availableSensors; i++) {
            final int offset = buffer.get();
            final int type = buffer.get();

            final float value = buffer.getFloat();

            packet.addSensor(sensorFactory.createDefaultSensor(type, value));
        }

        packet.setDate(new Date());

        return packet;
    }

    /**
     * Interpret bytes of a handshake packet. This method parse raw data into a structured java object.
     *
     * @param buffer raw data buffer.
     * @return interpreted handshake packet.
     */
    private Handshake interpretHandshake(final ByteBuffer buffer) {
        final int baudRate = buffer.getShort();
        //final long uptime = buffer.getInt();

        final int phraseLength = buffer.get();

        final byte[] phraseBytes = new byte[phraseLength];
        buffer.get(phraseBytes);
        final String phrase = new String( phraseBytes, Charset.forName("UTF-8"));

        final int versionLength = buffer.get();
        final byte[] versionBytes = new byte[versionLength];
        buffer.get(versionBytes);
        final String version = new String( versionBytes, Charset.forName("UTF-8"));

        final int nameLength = buffer.get();
        final byte[] nameBytes = new byte[nameLength];
        buffer.get(nameBytes);
        final String name = new String( nameBytes, Charset.forName("UTF-8"));


        return new Handshake(port.getPortName(), version,name, phrase, baudRate);
    }

    /**
     * Internal status enumeration.
     */
    private enum InternalStatus {
        IS_WAITING_FOR_PACKET,
        IS_WAITING_FOR_HANDSHAKE,
        IS_WAITING_FOR_KEEPALIVE,
        IS_WAITING_FOR_WAKE_UP,
        IS_WAITING_FOR_ACK,
        IS_READY
    }


    public void onHandshakeRequest(final HandshakeRequestCallbackEvent request) {
        LOGGER.debug("HandshakeRequestCallbackEvent received by {}", getClass().getCanonicalName());
        if (hRequest != null) {
            request.set(new StatusResponse<>(-2, "Handshake request on port '" + port + "' is pending. Cannot perform simultaneous handshakes.", null));
            return;
        }

        hRequest = request;
        try {
            send(requestFactory.createHandshakeRequest());
        } catch (final SerialException e) {
            request.set(new StatusResponse<>(-1, "Cannot perform handshake request on port '" + port + "'", null));
            hRequest = null;
        }

    }
}
