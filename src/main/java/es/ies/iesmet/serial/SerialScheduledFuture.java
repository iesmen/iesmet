package es.ies.iesmet.serial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;

/**
 * Created by jtorregrosa on 22/03/15.
 */
public class SerialScheduledFuture<T> implements Future<T> {
    /**
     * Internal Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(SerialManagerImpl.class);


    private T entity;
    private boolean isCancelled;
    private Callable<T> callable;
    private CountDownLatch latch = new CountDownLatch(1);

    public SerialScheduledFuture(final Callable<T> callable) {
        this.entity = null;
        this.isCancelled = false;
        this.callable = callable;
    }

    @Override
    public boolean cancel(final boolean mayInterruptIfRunning) {
        isCancelled = true;

        return true;
    }

    @Override
    public boolean isCancelled() {
        return isCancelled;
    }

    @Override
    public boolean isDone() {
        return entity != null;
    }

    @Override
    public T get() throws InterruptedException, ExecutionException {
        if (entity == null && !isCancelled) {
            LOGGER.trace("Locking thread...");
            latch.await();
            LOGGER.trace("Thread Unlocked");
        }

        return entity;
    }

    @Override
    public T get(final long timeout, final TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        if (entity == null && !isCancelled) {
            latch.await(timeout, unit);
        }

        return entity;
    }

    void setEntity(final T entity) {
        this.entity = entity;
        latch.countDown();
    }

    Callable<T> getTask() {
        return callable;
    }
}
