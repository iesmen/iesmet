/**
 * This file is part of IESmet.
 * <p/>
 * Foobar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * IESmet is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.ies.iesmet.serial;

import java.util.Collection;

/**
 * This interface defines a SerialManager object. It holds multiple serial connections. Only one connection per port
 * is supported.
 *
 * @author Jorge Torregrosa {@literal <jtorregrosalloret@gmail.com>}
 * @version 1.0
 */
public interface SerialManager {


    /**
     * Gets all available serial ports names.
     *
     * @return a collection of all available serial ports.
     * @throws SerialException if something goes wrong obtaining port list.
     */
    Collection<String> getAvailablePorts() throws SerialException;

    /**
     * Adds a new serial connection to the pool.
     * It tries to connect to the port and establish a connection.
     *
     * @param port port name.
     * @param baudRate connection baud rate.
     * @param dataBits connection data bits.
     * @param stopBits connection stop bits.
     * @param parity connection parity.
     * @throws SerialException if something goes wrong trying to establish the connection.
     */
    SerialConnection get(
            final String portName,
            final int baudRate,
            final DataBits dataBits,
            final StopBits stopBits,
            final Parity parity) throws SerialException;

    /**
     * Adds a new serial connection to the pool.
     * It tries to connect to the port and establish a connection.
     * Databits, stopbits and parity are defined as the default values. It can be different due to alternative
     * implementations.
     *
     * @param port port name.
     * @param baudRate connection baud rate.
     * @throws SerialException if something goes wrong trying to establish the connection.
     */
    SerialConnection get(final String portName, final int baudRate) throws SerialException;

    /**
     * The number of data bits in each character can be 5 (for Baudot code), 6 (rarely used), 7 (for true ASCII),
     * 8 (for most kinds of data, as this size matches the size of a byte), or 9 (rarely used).
     * 8 data bits are almost universally used in newer applications.
     * 5 or 7 bits generally only make sense with older equipment such as teleprinters.
     */
    public enum DataBits {
        DB_5(5),
        DB_6(6),
        DB_7(7),
        DB_8(8);

        private int value;

        private DataBits(final int value) {
            this.value = value;
        }

        /**
         * Gets internal value.
         *
         * @return internal value.
         */
        int getValue() {
            return value;
        }
    }

    /**
     * Stop bits sent at the end of every character allow the receiving signal hardware to detect the end of a
     * character and to resynchronise with the character stream. Electronic devices usually use one stop bit.
     * If slow electromechanical teleprinters are used, one-and-one half or two stop bits are required.
     */
    public enum StopBits {
        SB_1(1),
        SB_2(2),
        SB_1_5(3);

        private int value;

        private StopBits(final int value) {
            this.value = value;
        }

        /**
         * Gets internal value.
         *
         * @return internal value.
         */
        int getValue() {
            return value;
        }
    }

    /**
     * Parity is a method of detecting errors in transmission. When parity is used with a serial port, an extra
     * data bit is sent with each data character, arranged so that the number of 1 bits in each character,
     * including the parity bit, is always odd or always even.
     * If a byte is received with the wrong number of 1s, then it must have been corrupted. However, an even number of
     * errors can pass the parity check.
     */
    public enum Parity {
        P_NONE(0),
        P_ODD(1),
        P_EVEN(2),
        P_MARK(3),
        P_SPACE(4);

        private int value;

        private Parity(final int value) {
            this.value = value;
        }

        /**
         * Gets internal value.
         *
         * @return internal value.
         */
        int getValue() {
            return value;
        }


    }
}
