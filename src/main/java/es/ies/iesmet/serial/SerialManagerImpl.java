/**
 * This file is part of IESmet.
 * <p/>
 * Foobar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * IESmet is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.ies.iesmet.serial;

import es.ies.iesmet.serial.entity.RequestFactory;
import es.ies.iesmet.serial.entity.SensorFactory;
import es.ies.iesmet.serial.event.EventBusWrapper;
import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Collection;
import java.util.Properties;

/**
 * This is the default Serial Pool implementation. It holds multiple serial connections.
 *
 * @author Jorge Torregrosa {@literal <jtorregrosalloret@gmail.com>}
 * @version 1.0
 */
public class SerialManagerImpl implements SerialManager {
    /**
     * Internal Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(SerialManagerImpl.class);

    /**
     * Global properties wrapper.
     */
    private final Properties properties;

    /**
     * Default Sensor Factory.
     */
    private final SensorFactory factory;

    /**
     * Default Request Factory.
     */
    private final RequestFactory requestFactory;

    private SerialConnection connection;

    private EventBusWrapper serialBus;

    /**
     * Constructs a default serial pool from global properties wrapper.
     *
     * @param properties properties wrapper.
     */
    public SerialManagerImpl(final Properties properties, final EventBusWrapper serialBus) {
        this.properties = properties;
        this.factory = new SensorFactory(properties);
        this.requestFactory = new RequestFactory(properties);
        this.serialBus = serialBus;
        this.connection = null;
    }

    @Override
    public Collection<String> getAvailablePorts() {
        return Arrays.asList(SerialPortList.getPortNames());
    }

    @Override
    synchronized public SerialConnection get(
            final String portName,
            final int baudRate,
            final DataBits dataBits,
            final StopBits stopBits,
            final Parity parity) throws SerialException {

        final SerialPort port = new SerialPort(portName);

        try {
            port.openPort();
            port.setParams(baudRate, dataBits.getValue(), stopBits.getValue(), parity.getValue());
            port.setEventsMask(SerialPort.MASK_RXCHAR + SerialPort.MASK_CTS + SerialPort.MASK_DSR);//Set mask

            final SerialConnection conn = new SerialConnection(port, baudRate, factory, requestFactory, serialBus);
            port.addEventListener(conn);
            serialBus.register(conn);
            connection = conn;

            LOGGER.info("Serial port {} opened successfully.", port.getPortName());
            return conn;
        } catch (SerialPortException ex) {
            throw new SerialException("Cannot open connection with: " + portName, ex);
        }


    }

    @Override
    public SerialConnection get(final String portName, final int baudRate) throws SerialException {
        return get(portName, baudRate, DataBits.DB_8, StopBits.SB_1, Parity.P_NONE);
    }


}
