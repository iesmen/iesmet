/**
 * This file is part of IESmet.
 * <p/>
 * Foobar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * IESmet is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.ies.iesmet.serial;

/**
 * @author Jorge Torregrosa {@literal <jtorregrosalloret@gmail.com>}
 * @version 1.0
 */
public class SerialException extends Exception {
    /**
     * Constructs a default exception providing a cause message.
     *
     * @param message cause message.
     */
    public SerialException(final String message) {
        super(message);
    }

    /**
     * Constructs a default exception providing a cause message and a throwable.
     *
     * @param message cause message.
     * @param throwable wrapped throwable.
     */
    public SerialException(final String message, final Throwable throwable) {
        super(message, throwable);
    }
}
