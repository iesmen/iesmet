/**
 * This file is part of IESmet.
 * <p/>
 * Foobar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * IESmet is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.ies.iesmet.serial.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * This entity class defines a sensor packet. It holds useful information about the sensors, like reception date,
 * internal value, etc.
 *
 * @author Jorge Torregrosa {@literal <jtorregrosalloret@gmail.com>}
 * @version 1.0
 */
public class SerialPacket {

    /**
     * Sensor list.
     */
    private List<Sensor> sensors;

    /**
     * Reception date.
     */
    private Date date;

    /**
     * Constructs an empty sensor packet.
     */
    public SerialPacket() {
        sensors = new ArrayList<>();
    }

    /**
     * Constructs an empty sensor packet.
     */
    public SerialPacket(final List<Sensor> sensors) {
       this.sensors = sensors;
    }

    /**
     * Adds a sensor to this packet.
     *
     * @param sensor sensor to be added.
     */
    public void addSensor(final Sensor sensor) {
        sensors.add(sensor);
    }

    /**
     * Obtain the number of sensors in this packet.
     *
     * @return sensor count.
     */
    public int getSensorCount() {
        return sensors.size();
    }

    /**
     * Obtain a collection of all sensors in this packet.
     *
     * @return sensor collection.
     */
    public Collection<Sensor> getSensors() {
        return new ArrayList<>(sensors);
    }

    /**
     * Gets the reception date.
     *
     * @return reception date.
     */
    public Date getDate() {
        return date;
    }

    /**
     * Sets the reception date.
     *
     * @param date new reception date.
     */
    public void setDate(final Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "SerialPacket{" +
                "sensors=" + sensors +
                ", date=" + date +
                '}';
    }
}
