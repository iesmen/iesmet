/**
 * This file is part of IESmet.
 * <p/>
 * Foobar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * IESmet is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.ies.iesmet.serial.entity;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by jorge on 23/01/15.
 */
public class RequestFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(RequestFactory.class);
    private final Properties properties;

    private final Map<Request.RequestType, byte[]> serialCodes;

    public RequestFactory(final Properties properties) {
        this.properties = properties;
        this.serialCodes = new HashMap<>();
        populateSerialCodes();
    }

    private void populateSerialCodes() {
        try {
            serialCodes.put(Request.RequestType.RT_HANDSHAKE, Hex.decodeHex(properties.getProperty("serial.command.handshake").toCharArray()));
            serialCodes.put(Request.RequestType.RT_KEEP_ALIVE, Hex.decodeHex(properties.getProperty("serial.command.keepalive").toCharArray()));
            serialCodes.put(Request.RequestType.RT_SENSOR_REQUEST, Hex.decodeHex(properties.getProperty("serial.command.sensorrequest").toCharArray()));
            serialCodes.put(Request.RequestType.RT_WAKE_UP, Hex.decodeHex(properties.getProperty("serial.command.wakeup").toCharArray()));
            serialCodes.put(Request.RequestType.RT_GENERIC1, Hex.decodeHex(properties.getProperty("serial.command.generic1").toCharArray()));
            serialCodes.put(Request.RequestType.RT_GENERIC2, Hex.decodeHex(properties.getProperty("serial.command.generic2").toCharArray()));
            serialCodes.put(Request.RequestType.RT_GENERIC3, Hex.decodeHex(properties.getProperty("serial.command.generic3").toCharArray()));
            serialCodes.put(Request.RequestType.RT_GENERIC4, Hex.decodeHex(properties.getProperty("serial.command.generic4").toCharArray()));
            serialCodes.put(Request.RequestType.RT_GENERIC5, Hex.decodeHex(properties.getProperty("serial.command.generic5").toCharArray()));
        } catch (DecoderException ex) {
            LOGGER.error("Cannot load sensor serial code due to an error parsing configuration file.", ex);
        }
    }

    public Request createHandshakeRequest() {
        return new RequestImpl(Request.RequestType.RT_HANDSHAKE, serialCodes.get(Request.RequestType.RT_HANDSHAKE));
    }

    public Request createWakeUpRequest() {
        return new RequestImpl(Request.RequestType.RT_WAKE_UP, serialCodes.get(Request.RequestType.RT_WAKE_UP));
    }

    public Request createKeepAliveRequest() {
        return new RequestImpl(Request.RequestType.RT_KEEP_ALIVE, serialCodes.get(Request.RequestType.RT_KEEP_ALIVE));
    }

    public Request createSensorRequest() {
        return new RequestImpl(Request.RequestType.RT_SENSOR_REQUEST, serialCodes.get(Request.RequestType.RT_SENSOR_REQUEST));
    }

    public Request createGeneric1Request() {
        return new RequestImpl(Request.RequestType.RT_GENERIC1, serialCodes.get(Request.RequestType.RT_GENERIC1));
    }

    public Request createGeneric2Request() {
        return new RequestImpl(Request.RequestType.RT_GENERIC2, serialCodes.get(Request.RequestType.RT_GENERIC2));
    }

    public Request createGeneric3Request() {
        return new RequestImpl(Request.RequestType.RT_GENERIC3, serialCodes.get(Request.RequestType.RT_GENERIC3));
    }

    public Request createGeneric4Request() {
        return new RequestImpl(Request.RequestType.RT_GENERIC4, serialCodes.get(Request.RequestType.RT_GENERIC4));
    }

    public Request createGeneric5Request() {
        return new RequestImpl(Request.RequestType.RT_GENERIC5, serialCodes.get(Request.RequestType.RT_GENERIC5));
    }

}
