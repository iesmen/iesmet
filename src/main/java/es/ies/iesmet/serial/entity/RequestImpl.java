/**
 * This file is part of IESmet.
 * <p/>
 * Foobar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * IESmet is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.ies.iesmet.serial.entity;

/**
 * Created by jorge on 27/01/15.
 */
public class RequestImpl implements Request {
    private final byte[] serialCodes;
    private final RequestType type;

    RequestImpl(final RequestType type, byte... serialCodes) {
        this.serialCodes = serialCodes;
        this.type = type;
    }

    @Override
    public byte[] getSerialCodes() {
        return serialCodes;
    }

    @Override
    public RequestType getType() {
        return type;
    }
}
