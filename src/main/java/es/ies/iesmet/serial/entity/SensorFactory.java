/**
 * This file is part of IESmet.
 * <p/>
 * Foobar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * IESmet is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.ies.iesmet.serial.entity;

import es.ies.iesmet.serial.interpretation.Interpreter;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by jorge on 23/01/15.
 */
public class SensorFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(SensorFactory.class);
    private final Properties properties;

    private final Map<Integer, Sensor.SensorType> serialCodes;

    public SensorFactory(final Properties properties) {
        this.properties = properties;
        this.serialCodes = new HashMap<>();
        populateSerialCodes();
    }

    private void populateSerialCodes() {
        try {
            serialCodes.put((int) ByteBuffer.wrap(Hex.decodeHex(properties.getProperty("sensor.raingauge.code").toCharArray())).get(), Sensor.SensorType.RAIN_GAUGE);
            serialCodes.put((int) ByteBuffer.wrap(Hex.decodeHex(properties.getProperty("sensor.pressure.code").toCharArray())).get(), Sensor.SensorType.PRESSURE);
            serialCodes.put((int) ByteBuffer.wrap(Hex.decodeHex(properties.getProperty("sensor.temperature.code").toCharArray())).get(), Sensor.SensorType.TEMPERATURE);
            serialCodes.put((int) ByteBuffer.wrap(Hex.decodeHex(properties.getProperty("sensor.winddirection.code").toCharArray())).get(), Sensor.SensorType.WIND_DIRECTION);
            serialCodes.put((int) ByteBuffer.wrap(Hex.decodeHex(properties.getProperty("sensor.unknown.code").toCharArray())).get(), Sensor.SensorType.UNKNOWN);
            serialCodes.put((int) ByteBuffer.wrap(Hex.decodeHex(properties.getProperty("sensor.windspeed.code").toCharArray())).get(), Sensor.SensorType.WIND_SPEED);
            serialCodes.put((int) ByteBuffer.wrap(Hex.decodeHex(properties.getProperty("sensor.humidity.code").toCharArray())).get(), Sensor.SensorType.HUMIDITY);
        } catch (DecoderException ex) {
            LOGGER.error("Cannot load sensor serial code due to an error parsing configuration file.", ex);
        }
    }

    public Sensor createDefaultRainGaugeSensor(final float value) {
        String name = properties.getProperty("sensor.raingauge.name");
        String description = properties.getProperty("sensor.raingauge.description");
        String unit = properties.getProperty("sensor.raingauge.unit");
        String interpreterClass = properties.getProperty("sensor.raingauge.interpreter");

        Interpreter interpreter = null;
        try {
            Class<?> clazz = Class.forName(interpreterClass);

            interpreter = (Interpreter) clazz.newInstance();

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            LOGGER.error("Cannot instanciate interpreter '{}'. Leaving it without any interpreter.", interpreterClass);
        }

        return new SensorImpl(name, unit, description, Sensor.SensorType.RAIN_GAUGE, interpreter, value);
    }

    public Sensor createDefaultWindSpeedSensor(final float value) {
        String name = properties.getProperty("sensor.windspeed.name");
        String description = properties.getProperty("sensor.windspeed.description");
        String unit = properties.getProperty("sensor.windspeed.unit");
        String interpreterClass = properties.getProperty("sensor.windspeed.interpreter");

        Interpreter interpreter = null;
        try {
            Class<?> clazz = Class.forName(interpreterClass);

            interpreter = (Interpreter) clazz.newInstance();

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            LOGGER.error("Cannot instanciate interpreter '{}'. Leaving it without any interpreter.", interpreterClass);
        }

        return new SensorImpl(name, unit, description, Sensor.SensorType.WIND_SPEED, interpreter, value);
    }

    public Sensor createDefaultHumiditySensor(final float value) {
        String name = properties.getProperty("sensor.humidity.name");
        String description = properties.getProperty("sensor.humidity.description");
        String unit = properties.getProperty("sensor.humidity.unit");
        String interpreterClass = properties.getProperty("sensor.humidity.interpreter");

        Interpreter interpreter = null;
        try {
            Class<?> clazz = Class.forName(interpreterClass);

            interpreter = (Interpreter) clazz.newInstance();

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            LOGGER.error("Cannot instanciate interpreter '{}'. Leaving it without any interpreter.", interpreterClass);
        }

        return new SensorImpl(name, unit, description, Sensor.SensorType.HUMIDITY, interpreter, value);
    }

    public Sensor createDefaultPressureSensor(final float value) {
        String name = properties.getProperty("sensor.pressure.name");
        String description = properties.getProperty("sensor.pressure.description");
        String unit = properties.getProperty("sensor.pressure.unit");
        String interpreterClass = properties.getProperty("sensor.pressure.interpreter");

        Interpreter interpreter = null;
        try {
            Class<?> clazz = Class.forName(interpreterClass);

            interpreter = (Interpreter) clazz.newInstance();

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            LOGGER.error("Cannot instanciate interpreter '{}'. Leaving it without any interpreter.", interpreterClass);
        }

        return new SensorImpl(name, unit, description, Sensor.SensorType.PRESSURE, interpreter, value);
    }

    public Sensor createDefaultTemperatureSensor(final float value) {
        String name = properties.getProperty("sensor.temperature.name");
        String description = properties.getProperty("sensor.temperature.description");
        String unit = properties.getProperty("sensor.temperature.unit");
        String interpreterClass = properties.getProperty("sensor.temperature.interpreter");

        Interpreter interpreter = null;
        try {
            Class<?> clazz = Class.forName(interpreterClass);

            interpreter = (Interpreter) clazz.newInstance();

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            LOGGER.error("Cannot instanciate interpreter '{}'. Leaving it without any interpreter.", interpreterClass);
        }

        return new SensorImpl(name, unit, description, Sensor.SensorType.TEMPERATURE, interpreter, value);
    }

    public Sensor createDefaultWindDirectionSensor(final float value) {
        String name = properties.getProperty("sensor.winddirection.name");
        String description = properties.getProperty("sensor.winddirection.description");
        String unit = properties.getProperty("sensor.winddirection.unit");
        String interpreterClass = properties.getProperty("sensor.winddirection.interpreter");

        Interpreter interpreter = null;
        try {
            Class<?> clazz = Class.forName(interpreterClass);

            interpreter = (Interpreter) clazz.newInstance();

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            LOGGER.error("Cannot instanciate interpreter '{}'. Leaving it without any interpreter.", interpreterClass);
        }

        return new SensorImpl(name, unit, description, Sensor.SensorType.WIND_DIRECTION, interpreter, value);
    }

    public Sensor createDefaultUnknownSensor(final float value) {
        String name = properties.getProperty("sensor.unknown.name");
        String description = properties.getProperty("sensor.unknown.description");
        String unit = properties.getProperty("sensor.unknown.unit");
        String interpreterClass = properties.getProperty("sensor.unknown.interpreter");

        Interpreter interpreter = null;
        try {
            Class<?> clazz = Class.forName(interpreterClass);

            interpreter = (Interpreter) clazz.newInstance();

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            LOGGER.error("Cannot instanciate interpreter '{}'. Leaving it without any interpreter.", interpreterClass);
        }

        return new SensorImpl(name, unit, description, Sensor.SensorType.UNKNOWN, interpreter, value);
    }

    public Sensor createDefaultSensor(final int serialCode, final float value) {
        final Sensor.SensorType type = serialCodes.get(serialCode);

        switch (type) {
            case RAIN_GAUGE:
                return createDefaultRainGaugeSensor(value);
            case PRESSURE:
                return createDefaultPressureSensor(value);
            case TEMPERATURE:
                return createDefaultTemperatureSensor(value);
            case WIND_DIRECTION:
                return createDefaultWindDirectionSensor(value);
            case WIND_SPEED:
                return createDefaultWindSpeedSensor(value);
            case HUMIDITY:
                return createDefaultHumiditySensor(value);
            case UNKNOWN:
                return createDefaultUnknownSensor(value);

        }

        return null;
    }
}
