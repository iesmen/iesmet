/**
 * This file is part of IESmet.
 * <p/>
 * Foobar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * IESmet is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.ies.iesmet.serial.entity;

import es.ies.iesmet.serial.interpretation.Interpreter;

/**
 * This interface defines a Sensor entity in IESmet system.
 *
 * <p>A sensor is an entity that represent a real sensor in
 * an Arduino board.</p>
 *
 * @author Jorge Torregrosa {@literal <jtorregrosalloret@gmail.com>}
 */
public interface Sensor {

    /**
     * Enumerates available sensor types.
     */
    public enum SensorType {
        PRESSURE,
        RAIN_GAUGE,
        TEMPERATURE,
        WIND_DIRECTION,
        WIND_SPEED,
        HUMIDITY,
        UNKNOWN
    }

    /**
     * Gets sensor name.
     *
     * <p>This value is for representation purposes only.</p>
     *
     * @return sensor name.
     */
    String getName();

    /**
     * Gets sensor unit.
     *
     * <p>This value is for representation purposes only.</p>
     *
     * @return sensor unit.
     */
    String getUnit();

    /**
     * Gets sensor description.
     *
     * <p>This value is for representation purposes only.</p>
     *
     * @return sensor description.
     */
    String getDescription();

    /**
     * Gets sensor value.
     *
     * <p>Value type is parametrized. This value is already interpreted by the associated interpreter if exists.</p>
     *
     * @return sensor value.
     */
    double getValue();

    /**
     * Gets sensor value.
     *
     * <p>Value type is parametrized.</p>
     *
     * @return sensor value.
     */
    float getRawValue();

    /**
     * Gets sensor type.
     *
     * <p>This type is used in serial communication to identify sensor type.</p>
     *
     * @return sensor type.
     */
    SensorType getSensorType();

    /**
     * Gets interpreter used to interpret sensor value.
     *
     * @return associated interpreter.
     */
    Interpreter getInterpreter();

    /**
     * Sets interpreter used to interpret sensor value.
     *
     * @param interpreter new associated interpreter.
     */
    void setInterpreter(Interpreter interpreter);

}
