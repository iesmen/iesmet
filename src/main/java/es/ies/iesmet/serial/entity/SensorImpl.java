/**
 * This file is part of IESmet.
 * <p/>
 * Foobar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * IESmet is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.ies.iesmet.serial.entity;

import es.ies.iesmet.serial.interpretation.Interpreter;

/**
 * This class implements a Sensor entity behaviour in IESmet system.
 *
 * <p>A sensor is an entity that represent a real sensor in
 * an Arduino board.</p>
 *
 * @author Jorge Torregrosa {@literal <jtorregrosalloret@gmail.com>}
 * @see es.ies.iesmet.serial.entity.Sensor
 * @see java.lang.Number
 */
public class SensorImpl implements Sensor {

    /**
     * Sensor name.
     */
    private final String name;

    /**
     * Sensor unit.
     */
    private final String unit;

    /**
     * Sensor description.
     */
    private final String description;

    /**
     * Sensor value.
     */
    private final float value;

    /**
     * Sensor associated serial code.
     */
    private final SensorType sensorType;

    /**
     * Associated sensor interpreter.
     */
    private Interpreter interpreter;

    /**
     * Constructs a sensor providing a name, a unit, a description and a sensor type.
     *
     * @param name sensor name.
     * @param unit sensor unit.
     * @param description sensor description.
     * @param sensorType sensor type.
     */
    public SensorImpl(final String name, final String unit, final String description, final SensorType sensorType) {
        this(name, unit, description, sensorType, null);
    }

    /**
     * Constructs a sensor providing a name, a unit, a description, a sensor type and an interpreter.
     *
     * @param name sensor name.
     * @param unit sensor unit.
     * @param description sensor description.
     * @param sensorType sensor type.
     * @param interpreter associated interpreter.
     */
    public SensorImpl(final String name, final String unit, final String description, final SensorType sensorType, final Interpreter interpreter) {
        this(name, unit, description, sensorType, interpreter, 0);
    }

    /**
     * Constructs a complete sensor providing a name, a unit, a description, a sensor type, an interpreter and
     * a initialization value.
     *
     * @param name sensor name.
     * @param unit sensor unit.
     * @param description sensor description.
     * @param sensorType sensor type.
     * @param interpreter associated interpreter.
     * @param value sensor value.
     */
    public SensorImpl(final String name, final String unit, final String description, final SensorType sensorType, final Interpreter interpreter, final float value) {
        this.name = name;
        this.unit = unit;
        this.description = description;
        this.value = value;
        this.interpreter = interpreter;
        this.sensorType = sensorType;
    }


    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getUnit() {
        return unit;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public double getValue() {
        return interpreter.interpret(value);
    }

    @Override
    public float getRawValue() {
        return value;
    }

    @Override
    public SensorType getSensorType() {
        return sensorType;
    }

    @Override
    public Interpreter getInterpreter() {
        return interpreter;
    }

    @Override
    public void setInterpreter(Interpreter interpreter) {
        this.interpreter = interpreter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SensorImpl sensor = (SensorImpl) o;

        return sensorType == sensor.sensorType && name.equals(sensor.name) && value == sensor.value;
    }

    @Override
    public int hashCode() {
        int result = (value != +0.0f ? Float.floatToIntBits(value) : 0);
        result = 31 * result + sensorType.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "SensorImpl{" +
                "name='" + name + '\'' +
                ", unit='" + unit + '\'' +
                ", description='" + description + '\'' +
                ", value=" + value +
                ", sensorType=" + sensorType +
                ", interpreter=" + interpreter +
                '}';
    }
}
