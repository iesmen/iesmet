/**
 * This file is part of IESmet.
 * <p/>
 * Foobar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * IESmet is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.ies.iesmet.serial.entity;

/**
 * Created by jorge on 27/01/15.
 */
public class Handshake {
    private final String libraryVersion;
    private final String boardName;
    private final String phrase;
    private final int baudRate;
    private final String portName;

    public Handshake(final String portName, final String libraryVersion, final String boardName, final String phrase, final int baudRate) {
        this.libraryVersion = libraryVersion;
        this.boardName = boardName;
        this.baudRate = baudRate;
        this.phrase = phrase;
        this.portName = portName;
    }

    public String getLibraryVersion() {
        return libraryVersion;
    }

    public String getBoardName() {
        return boardName;
    }

    public int getBaudRate() {
        return baudRate;
    }

    public String getPhrase() {
        return phrase;
    }

    public String getPortName() {
        return portName;
    }

    @Override
    public String toString() {
        return "Handshake{" +
                "libraryVersion='" + libraryVersion + '\'' +
                ", boardName='" + boardName + '\'' +
                ", phrase='" + phrase + '\'' +
                ", baudRate=" + baudRate +
                ", portName='" + portName + '\'' +
                '}';
    }
}
