/**
 * This file is part of IESmet.
 * <p/>
 * Foobar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * IESmet is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.ies.iesmet.serial.entity;

/**
 * Created by jorge on 27/01/15.
 */
public interface Request {
    public enum RequestType {
        RT_KEEP_ALIVE,
        RT_WAKE_UP,
        RT_SENSOR_REQUEST,
        RT_HANDSHAKE,
        RT_GENERIC1,
        RT_GENERIC2,
        RT_GENERIC3,
        RT_GENERIC4,
        RT_GENERIC5

    }

    byte[] getSerialCodes();

    RequestType getType();
}
