package es.ies.iesmet.module;

import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import es.ies.iesmet.event.BeginSchedulerModuleEvent;
import es.ies.iesmet.injection.annotation.ControllerBus;
import es.ies.iesmet.injection.annotation.SerialBus;
import es.ies.iesmet.serial.entity.RequestFactory;
import es.ies.iesmet.serial.event.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by alberto on 6/04/15.
 */
public class RequestSchedulerModule extends DaemonModule{

    public static String MODULE_NAME = "RequestSchedulerModule";
    private static final Logger LOGGER = LoggerFactory.getLogger(RequestSchedulerModule.class);

   @Inject
   @SerialBus
   private EventBusWrapper serialBus;

    @Inject
    @ControllerBus
    private EventBusWrapper controllerBus;

    @Inject
    private Properties prop;

    private ScheduledExecutorService service;
    private RequestFactory reqFactory;

    private AtomicBoolean isReady;
    private AtomicBoolean isInitializated;
    private long lastRequestTime;

    @Override
    public void initialize() {
        service = Executors.newSingleThreadScheduledExecutor();
        reqFactory = new RequestFactory(prop);
        isReady = new AtomicBoolean(true);
        lastRequestTime = -1;
        isInitializated = new AtomicBoolean(false);
    }

    @Override
    public String getModuleName() {
        return MODULE_NAME;
    }

    @Override
    public void run(){
        service.scheduleAtFixedRate(() -> {
            if(isInitializated.get() && isReady.get() && (lastRequestTime < 0 || (new Date()).getTime() - lastRequestTime > 30000)){
                isReady.lazySet(false);
                serialBus.post(new Generic1Event());
                lastRequestTime = (new Date()).getTime();
            }

            if (isInitializated.get() && isReady.get()) {
                isReady.lazySet(false);
                serialBus.post(new SensorRequestEvent(reqFactory.createSensorRequest()));
            }
        }, 0, 1000, TimeUnit.MILLISECONDS);
    }

    @Override
    public void stopModule() {
        service.shutdown();
        super.stopModule();
    }

    @Subscribe
    public void onACKReception(final ACKEvent e){
        isReady.lazySet(true);
    }

    @Subscribe
    public void onPacketReception(final SerialPacketEvent e){
        isReady.lazySet(true);
    }

    @Subscribe
    public void onBeginEvent(final BeginSchedulerModuleEvent e) {
        isInitializated.lazySet(true);
    }

}
