package es.ies.iesmet.module;

import javafx.concurrent.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * Created by jtorregrosa on 26/04/15.
 */
public abstract class DaemonModule implements Module, Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(DaemonModule.class);

    private final ExecutorService service;



    public DaemonModule() {
        this.service = Executors.newSingleThreadExecutor(new DaemonFactory());
    }

    @Override
    public void startModule() {
        LOGGER.info("Starting daemonized module {}...", getModuleName());
        service.execute(this);
    }

    @Override
    public void stopModule() {
        LOGGER.info("Stopping daemonized module {}...", getModuleName());
        service.shutdown();
    }

    @Override
    public boolean isModuleRunning(){
        return !service.isShutdown();
    }


    class DaemonFactory implements ThreadFactory {
        public Thread newThread(final Runnable r) {
            final Thread t = Executors.defaultThreadFactory().newThread(r);
            t.setDaemon(true);
            t.setName(getModuleName());
            return t;
        }
    }



}
