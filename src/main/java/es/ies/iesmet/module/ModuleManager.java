package es.ies.iesmet.module;

import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import com.google.inject.Injector;
import es.ies.iesmet.event.GetInjectorEvent;
import es.ies.iesmet.event.StopAllModulesEvent;
import es.ies.iesmet.event.StopModuleEvent;
import es.ies.iesmet.injection.annotation.ControllerBus;
import es.ies.iesmet.serial.event.EventBusWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alberto on 25/04/15.
 */
public class ModuleManager implements AutoCloseable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ModuleManager.class);

    @Inject
    @ControllerBus
    private EventBusWrapper controllerbus;

    @Inject
    private Injector injector;

    private List<Module> modules;

    private boolean isStarted;

    public ModuleManager() {
        modules = new ArrayList<Module>();
        isStarted = false;
    }

    public void register(final Module module) {
        if(modules.stream().anyMatch(a -> a.getModuleName().equals(module.getModuleName()))) {
            throw new IllegalArgumentException("Module identificator is already registered");
        }
        modules.add(module);
    }

    public void unRegister(final Module module) {
        modules.remove(module);
    }

    public Module get(String name) {
        for(Module module: modules) {
            if(module.getModuleName().equals(name))
                return module;
        }
        return null;
    }

    public void startAll() throws InterruptedException {
        modules.forEach(Module::initialize);
        modules.forEach(Module::startModule);
        if(!isStarted) {
            isStarted = true;
            LOGGER.debug("Starting ModuleManager thread");
            run();
        }
    }

    public void startModule(final String name){
        modules.stream().filter(module -> module.getModuleName().equals(name)).forEach(x -> {
            x.initialize();
            x.startModule();
        });
        if(!isStarted) {
            isStarted = true;
            LOGGER.debug("Starting ModuleManager thread");
            run();
        }
    }

    public void stopModule(final String name){
        modules.stream().filter(module -> module.getModuleName().equals(name)).forEach(es.ies.iesmet.module.Module::stopModule);
    }

    @Override
    public void close() throws Exception {
        modules.forEach(Module::stopModule);
    }

    private boolean _areAnyModuleRunning(){
        for(final Module module : modules){
            if(module.isModuleRunning()) {
                return true;
            }
        }
        return false;
    }


    private void run(){
        while(_areAnyModuleRunning() ){
            //LOGGER.trace("run");
        }
        isStarted = false;
        LOGGER.info("ModuleManager exits successfully");
    }

    @Subscribe
    public void onStopAllModules(final StopAllModulesEvent e){
        LOGGER.debug("StopAllModulesEvent received by {}", getClass().getCanonicalName());
        try {
            close();
        } catch (final Exception ex) {
            LOGGER.error("Cannot stop all modules.", ex);
        }
    }

    @Subscribe
    public void onStopModule(final StopModuleEvent e){
        LOGGER.debug("StopModuleEvent received by {}", getClass().getCanonicalName());
        try {
            modules.stream().filter(module -> module.getModuleName().equals(e.get())).forEach(es.ies.iesmet.module.Module::stopModule);
        } catch (final Exception ex) {
            LOGGER.error("Cannot stop module '" + e.get() + "'", ex);
        }
    }

    @Subscribe
    public void onGetInjector(final GetInjectorEvent e){
        e.set(injector);
    }
}
