package es.ies.iesmet.module;

import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import es.ies.iesmet.DAO.SensorDAO;
import es.ies.iesmet.DAO.SensorTypeDAO;
import es.ies.iesmet.injection.annotation.SerialBus;
import es.ies.iesmet.model.Sensor;
import es.ies.iesmet.model.SensorType;
import es.ies.iesmet.serial.entity.SerialPacket;
import es.ies.iesmet.serial.event.EventBusWrapper;
import es.ies.iesmet.serial.event.HandshakeEvent;
import es.ies.iesmet.serial.event.SerialPacketEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by alberto on 6/04/15.
 */
public class PersistenceModule extends DaemonModule{

    private static final Logger LOGGER = LoggerFactory.getLogger(PersistenceModule.class);

    public static String MODULE_NAME = "MongoDBPersistenceModule";

   @Inject
   @SerialBus
   private EventBusWrapper serialBus;


    public void addSensorRegistry(SensorType type, String value, String time) {
        SensorDAO dao = new SensorDAO();
        float newValue = Float.valueOf(value);
        Timestamp newTime = Timestamp.valueOf(time);
        Sensor sensor = new Sensor(newValue, newTime, type);
        System.out.println(sensor);
        dao.create(sensor);
    }

    public Sensor getSensorRegistry(SensorType type, String id) {
        SensorDAO dao = new SensorDAO();
        Sensor sensor = sensor = dao.getById(id);
        return sensor;
    }

    public SensorType getSensorType(String value) {
        SensorTypeDAO dao = new SensorTypeDAO();
        SensorType type = dao.getByValue(value);

        return type;
    }

    public void deleteRegistry(String id) {
        SensorDAO dao = new SensorDAO();
        dao.delete(id);
    }

    public List<Sensor> getAllRegistries() {
        SensorDAO dao = new SensorDAO();
        List<Sensor> sensors = dao.getAll();
        System.out.println("All:");
        for(Sensor sensor : sensors) {
            System.out.println(sensor);
        }
        return sensors;
    }

    @Override
    public void initialize() {

    }

    @Override
    public String getModuleName() {
        return MODULE_NAME;
    }

    @Subscribe
    public void onHandshake(final HandshakeEvent e){
        System.out.println("Persistence module reception " + e.get().toString());
    }

    @Subscribe
    public void receipt(SerialPacketEvent e) {
        SerialPacket packet = e.get();
        Collection<es.ies.iesmet.serial.entity.Sensor> sensors = packet.getSensors();
        Sensor sensor;
        Date now = new Date();
        SensorDAO dao = new SensorDAO();
        SensorTypeDAO typeDao = new SensorTypeDAO();
        SensorType type;
        for(es.ies.iesmet.serial.entity.Sensor sSensor: sensors) {
            type = typeDao.getByValue(sSensor.getSensorType().toString());
            sensor = new Sensor(sSensor.getValue(), new Timestamp(now.getTime()),type);
            dao.create(sensor);
        }
        LOGGER.info(e.get().toString());
    }

    @Override
    public void run(){
    }

    /*public void test() {
        Manager mgr = new Manager();
        //Sensor sensor = mgr.getSensor(SensorType.S_TEMPERATURE,"5522610ac49b427b4d1b3779");
        //System.out.println("Found: " + sensor);
        //SensorType type = mgr.getSensorType("TEMPERATURE");
        //System.out.println(type);
        //mgr.addSensorRegistry(type, "19.85", "2015-05-02 01:00:00.0");
        //List<Sensor> sensors = mgr.getAllRegistries();
        //mgr.deleteRegistry("553113a0e13c7a3ec9f66816");
        List<es.ies.iesmet.serial.entity.Sensor> sensors = new ArrayList<es.ies.iesmet.serial.entity.Sensor>();
        SensorImpl sImpl = new SensorImpl("TempS","01","Sensor for something", es.ies.iesmet.serial.entity.Sensor.SensorType.TEMPERATURE,4);
        SensorImpl sImpl2 = new SensorImpl("TempS2","02","Sensor for something", es.ies.iesmet.serial.entity.Sensor.SensorType.TEMPERATURE,5);
        sensors.add(sImpl);
        sensors.add(sImpl2);
        SerialPacket packet = new SerialPacket(sensors);
        mgr.receipt(packet);
    }*/
}
