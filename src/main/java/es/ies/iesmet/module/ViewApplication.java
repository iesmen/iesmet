package es.ies.iesmet.module;

import com.google.inject.Inject;
import com.google.inject.Injector;
import es.ies.iesmet.controller.BootstrapController;
import es.ies.iesmet.event.Event;
import es.ies.iesmet.serial.event.EventBusWrapper;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

/**
 * Created by jtorregrosa on 26/04/15.
 */
public class ViewApplication extends Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(ViewApplication.class);

    /**
     * Bootstrap window fxml file.
     */
    private static final String FXML_FILE = "/fxml/bootstrap.fxml";
    private final Injector injector;

    private BootstrapController ctrl;

    /**
     * Window width.
     */
    private static final int WIDTH = 620;

    /**
     * Window height.
     */
    private static final int HEIGHT = 480;

    public ViewApplication(final Injector injector){
        this.injector = injector;
    }

    @Override
    public void start(Stage stage) throws Exception {
        ctrl = injector.getInstance(BootstrapController.class);
        try (final InputStream stream = getClass().getResourceAsStream(FXML_FILE)) {
            ctrl.setStage(stage);
            final FXMLLoader loader = new FXMLLoader();
            loader.setControllerFactory(param -> ctrl);

            final Parent rootNode = loader.load(stream);

            LOGGER.debug("Showing JFX scene");
            final Scene scene = new Scene(rootNode, WIDTH, HEIGHT);

            stage.setResizable(false);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setTitle("IESmet - 1.0");
            stage.setScene(scene);
            stage.show();
        }
    }
}
