package es.ies.iesmet.module;

import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import es.ies.iesmet.event.StatusResponse;
import es.ies.iesmet.injection.annotation.SerialBus;
import es.ies.iesmet.serial.SerialConnection;
import es.ies.iesmet.serial.SerialException;
import es.ies.iesmet.serial.SerialManager;
import es.ies.iesmet.serial.SerialManagerImpl;
import es.ies.iesmet.serial.event.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/**
 * Created by jtorregrosa on 24/04/15.
 */
public class SerialModule extends DaemonModule{

    private static final Logger LOGGER = LoggerFactory.getLogger(SerialModule.class);

    private static final String MODULE_NAME = "SerialModule";

    @Inject
    @SerialBus
    private EventBusWrapper serialBus;

    @Inject
    private Properties serialProps;


    private SerialManager mgr;
    private SerialConnection connection;

    public SerialModule() {
    }

    @Override
    public void run(){
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final SerialModule that = (SerialModule) o;

        return getModuleName().equals(that.getModuleName());

    }

    @Override
    public int hashCode() {
        return getModuleName().hashCode();
    }

    @Subscribe
    public void onGetAvailablePorts(final GetAvailablePortsEvent e) {
        LOGGER.debug("GetAvailablePortsEvent received by {}", getClass().getCanonicalName());
        try {
            e.set(new StatusResponse<>(0, null, mgr.getAvailablePorts()));
        } catch (final SerialException ex) {
            e.set(new StatusResponse<>(-1, "Cannot retrieve available port list", new ArrayList<>()));
            LOGGER.error("Cannot retrieve available port list.", ex);
        }
    }


    @Subscribe
    public void onEstablishConnection(final EstablishConnectionEvent e) {
        LOGGER.debug("onEstablishConnection received by {}", getClass().getCanonicalName());

        if (connection != null) {
            e.set(new StatusResponse<>(-2, "Cannot create connection to port '" + e.getPortName() + "' with a '" + e.getBaudRate() + "' baudrate. Only one simultaneous connection is allowed.", null));
            LOGGER.error("Cannot create connection to port '" + e.getPortName() + "' with a '" + e.getBaudRate() + "' baudrate. Only one simultaneous connection is allowed.");
        }

        try {
            connection = mgr.get(e.getPortName(), e.getBaudRate());
            final ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
            scheduledExecutorService.schedule(() -> e.set(new StatusResponse<>(0, null, null)), 5, TimeUnit.SECONDS);

        } catch (final SerialException ex) {
            e.set(new StatusResponse<>(-1, "Cannot create connection to port '" + e.getPortName() + "' with a '" + e.getBaudRate() + "' baudrate.", null));
            LOGGER.error("Cannot create connection to port '" + e.getPortName() + "' with a '" + e.getBaudRate() + "' baudrate.", ex);
        }
    }

    @Subscribe
    public void onSensorRequest(final SensorRequestEvent request) {
        LOGGER.debug("onSensorRequest received by {}", getClass().getCanonicalName());

        if(connection != null) {
            try {
                connection.send(connection.getRequestFactory().createSensorRequest());
            } catch (final SerialException ex) {
                LOGGER.error("Cannot perform sensor request on port '" + connection.getPortName() + "'", ex);
            }
        }
    }

    @Subscribe
    public void onHandshakeRequest(final HandshakeRequestCallbackEvent request) {
        LOGGER.debug("onHandshakeRequest received by {}", getClass().getCanonicalName());
        if(connection != null) {
            connection.onHandshakeRequest(request);
        }
    }

    @Subscribe
    public void onCloseConnection(final CloseConnectionEvent e) {
        LOGGER.debug("onCloseConnection received by {}", getClass().getCanonicalName());
        try {
            connection.close();
            connection = null;
            e.set(new StatusResponse<>(0, null, null));
        } catch (Exception e1) {
            e.set(new StatusResponse<>(-1, "Cannot close connection to port '" + connection.getPortName() + "' with a '" + connection.getBaudRate() + "' baudrate.", null));
            LOGGER.error("Cannot create connection to port '" + connection.getPortName() + "' with a '" + connection.getBaudRate() + "' baudrate.");
        }
    }

    @Subscribe
    public void onGeneric1(final Generic1Event e) {
        LOGGER.debug("onGeneric1 received by {}", getClass().getCanonicalName());
            try {
                connection.send(connection.getRequestFactory().createGeneric1Request());
            } catch (SerialException ex) {
                LOGGER.error("Cannot perform Generic1 request on port '" + connection.getPortName() + "'", ex);
            }

    }

    @Override
    public void initialize() {
        mgr = new SerialManagerImpl(serialProps, serialBus);
    }

    @Override
    public void stopModule() {
        if(connection!= null){
            try {
                connection.close();
                LOGGER.info("Closing opened connection on '{}'...", connection.getPortName());
            } catch (final Exception ex) {
                LOGGER.error("There is a problem trying to close connection.", ex);
            }
        }
        super.stopModule();
    }

    @Override
    public String getModuleName() {
        return MODULE_NAME;
    }

}
