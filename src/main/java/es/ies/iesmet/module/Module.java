package es.ies.iesmet.module;

/**
 * Created by jtorregrosa on 24/04/15.
 */
public interface Module {
    void initialize();

    void startModule();

    void stopModule();

    String getModuleName();

    boolean isModuleRunning();
}
