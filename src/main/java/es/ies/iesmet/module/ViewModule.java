package es.ies.iesmet.module;

import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import com.google.inject.Injector;
import es.ies.iesmet.controller.BootstrapController;
import es.ies.iesmet.injection.annotation.ControllerBus;
import es.ies.iesmet.injection.annotation.SerialBus;
import es.ies.iesmet.serial.SerialConnection;
import es.ies.iesmet.serial.SerialException;
import es.ies.iesmet.serial.SerialManager;
import es.ies.iesmet.serial.SerialManagerImpl;
import es.ies.iesmet.serial.event.*;
import es.ies.iesmet.util.FXLauncher;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by jtorregrosa on 24/04/15.
 */
public class ViewModule extends DaemonModule {

    private static final Logger LOGGER = LoggerFactory.getLogger(ViewModule.class);

    private static final String MODULE_NAME = "JavaFXViewModule";

    @Inject
    @SerialBus
    private EventBusWrapper serialBus;

    @Inject
    @ControllerBus
    private EventBusWrapper controllerBus;


    @Inject
    private Properties serialProps;

    @Inject
    private Injector ctrInjector;
    private boolean isRunning;

    public ViewModule() {
        this.isRunning = false;
    }

    @Override
    public void run(){
        LOGGER.info("Launching view...");
        FXLauncher.launchApplication(ViewApplication.class, new String[0], new ViewApplication(ctrInjector));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ViewModule that = (ViewModule) o;

        return getModuleName().equals(that.getModuleName());
    }

    @Override
    public int hashCode() {
        return getModuleName().hashCode();
    }


    @Override
    public void initialize() {

    }

    @Override
    public String getModuleName() {
        return MODULE_NAME;
    }

}
