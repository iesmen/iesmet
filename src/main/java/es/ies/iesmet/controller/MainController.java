package es.ies.iesmet.controller;

import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import com.google.inject.Injector;
import es.ies.iesmet.injection.annotation.SerialBus;
import es.ies.iesmet.serial.entity.Sensor;
import es.ies.iesmet.serial.entity.SensorImpl;
import es.ies.iesmet.serial.entity.SerialPacket;
import es.ies.iesmet.serial.event.EventBusWrapper;
import es.ies.iesmet.serial.event.SerialPacketEvent;
import es.ies.iesmet.serial.interpretation.DummyInterpreter;
import javafx.animation.AnimationTimer;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.StageStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;

/**
 * Created by jtorregrosa on 22/03/15.
 */
public class MainController implements Initializable {

    /**
     * Internal class logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);

    private static final String RAIN_FXML = "/fxml/rain-gauge.fxml";
    private static final String HUMIDITY_FXML = "/fxml/humidity.fxml";
    private static final String PRESSURE_FXML = "/fxml/pressure.fxml";
    private static final String WIND_DIRECTION_FXML = "/fxml/wind-direction.fxml";
    private static final String WIND_SPEED_FXML = "/fxml/wind-speed.fxml";
    private static final String TEMPERATURE_FXML = "/fxml/temperature.fxml";


    @Inject
    @SerialBus
    private EventBusWrapper serialBus;

    @Inject
    private Injector injector;

    @FXML
    private AnchorPane canvasPane;

    @FXML
    private Pane chartPane;


    private XYChart.Series series;
    private int xSeriesData = 0;

    private ConcurrentLinkedQueue<Number> dataRain = new ConcurrentLinkedQueue<>();
    private ConcurrentLinkedQueue<Number> dataPressure = new ConcurrentLinkedQueue<>();
    private ConcurrentLinkedQueue<Number> dataHumidity = new ConcurrentLinkedQueue<>();
    private ConcurrentLinkedQueue<Number> dataWinddir = new ConcurrentLinkedQueue<>();
    private ConcurrentLinkedQueue<Number> dataWindspd = new ConcurrentLinkedQueue<>();
    private ConcurrentLinkedQueue<Number> dataTemp = new ConcurrentLinkedQueue<>();

    private long pressureLastTime = -1;

    private Map<String, Parent> chartViews;

    public MainController() {
        this.dataRain = new ConcurrentLinkedQueue<>();
        this.dataPressure = new ConcurrentLinkedQueue<>();
        this.dataHumidity = new ConcurrentLinkedQueue<>();
        this.dataWinddir = new ConcurrentLinkedQueue<>();
        this.dataWindspd = new ConcurrentLinkedQueue<>();
        this.dataTemp = new ConcurrentLinkedQueue<>();
        chartViews = new HashMap<>();
    }

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        chartViews.put("rain",loadView(RAIN_FXML, dataRain, "Rain Gauge (mm)",1000l));
        chartViews.put("humidity", loadView(HUMIDITY_FXML, dataHumidity, "Humidity (%)",1000l));
        chartViews.put("temperature", loadView(TEMPERATURE_FXML, dataTemp, "Temperature (celsius)",1000l));
        chartViews.put("winddir", loadView(WIND_DIRECTION_FXML, dataWinddir, "Wind Direction (degree)",1000l));
        chartViews.put("windspeed", loadView(WIND_SPEED_FXML, dataWindspd, "Wind Speed (km/h)",1000l));
        chartViews.put("pressure", loadView(PRESSURE_FXML, dataPressure, "Pressure (mbar)",1000l));
    }

    @Subscribe
    public void onPacketReception(final SerialPacketEvent e) {
        final Collection<Sensor> sensors = e.get().getSensors();
        final long currentTime = new Date().getTime();
        for(final Sensor s : sensors){
            switch(s.getSensorType()){
                case PRESSURE:
                    //if(pressureLastTime < 0 || currentTime - pressureLastTime > 5000 ) {
                        dataPressure.add(s.getValue());
                        //pressureLastTime = currentTime;
                    //}
                    break;
                case RAIN_GAUGE:
                    dataRain.add(s.getValue());
                    break;
                case TEMPERATURE:
                    dataTemp.add(s.getValue());
                    break;
                case WIND_DIRECTION:
                    dataWinddir.add(s.getValue());
                    break;
                case WIND_SPEED:
                    dataWindspd.add(s.getValue());
                    break;
                case HUMIDITY:
                    dataHumidity.add(s.getValue());
                    break;
                case UNKNOWN:
                    break;
            }
        }
    }

    public void showRainChart() {
        chartPane.getChildren().removeAll(chartPane.getChildren());
        chartPane.getChildren().add(chartViews.get("rain"));
    }

    public void showHumidityChart() {
        chartPane.getChildren().removeAll(chartPane.getChildren());
        chartPane.getChildren().add(chartViews.get("humidity"));
    }

    public void showTemperatureChart() {
        chartPane.getChildren().removeAll(chartPane.getChildren());
        chartPane.getChildren().add(chartViews.get("temperature"));
    }

    public void showWindDirChart() {
        chartPane.getChildren().removeAll(chartPane.getChildren());
        chartPane.getChildren().add(chartViews.get("winddir"));
    }

    public void showWindSpeedChart() {
        chartPane.getChildren().removeAll(chartPane.getChildren());
        chartPane.getChildren().add(chartViews.get("windspeed"));
    }

    public void showPressureChart() {
        chartPane.getChildren().removeAll(chartPane.getChildren());
        chartPane.getChildren().add(chartViews.get("pressure"));
    }


    private Parent loadView(final String fxmlPath, final ConcurrentLinkedQueue<Number> data, final String name, final long updateMillis){
        final FXMLLoader loader = new FXMLLoader();

        final ChartController ctrl = injector.getInstance(ChartController.class);

        ctrl.setDataSeries(data);
        ctrl.setName(name);
        ctrl.setUpdateTime(updateMillis);

        loader.setControllerFactory(param -> ctrl);

        try (final InputStream stream = getClass().getResourceAsStream(fxmlPath)) {
            return loader.load(stream);
        } catch (final IOException ex) {
            LOGGER.error("Cannot load " + fxmlPath + ".", ex);
        }

        return null;
    }
}
