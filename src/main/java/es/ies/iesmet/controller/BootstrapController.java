package es.ies.iesmet.controller;

import com.google.inject.Inject;
import com.google.inject.Injector;
import es.ies.iesmet.event.BeginSchedulerModuleEvent;
import es.ies.iesmet.event.StopAllModulesEvent;
import es.ies.iesmet.injection.annotation.ControllerBus;
import es.ies.iesmet.injection.annotation.SerialBus;
import es.ies.iesmet.serial.event.EstablishConnectionEvent;
import es.ies.iesmet.serial.event.EventBusWrapper;
import es.ies.iesmet.serial.event.GetAvailablePortsEvent;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

public class BootstrapController implements Initializable {

    /**
     * Internal class logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(BootstrapController.class);
    private static final String TESTING_FXML = "/fxml/testing-connection.fxml";
    private static final String MAIN_FXML = "/fxml/main.fxml";

    @Inject
    private Properties properties;
    private boolean connectionTest;
    @FXML
    private TextField baudRate;
    @FXML
    private ComboBox<String> portList;
    @FXML
    private Button testButton;
    @FXML
    private Button connectButton;
    @FXML
    private Button closeButton;
    @Inject
    @ControllerBus
    private EventBusWrapper controllerBus;

    @Inject
    @SerialBus
    private EventBusWrapper serialBus;

    @Inject
    private Injector injector;


    private Stage assocStage;

    public BootstrapController() {
        this.connectionTest = false;
    }

    private void setDefaults() {
        baudRate.setText(properties.getProperty("serial.system.baudrate"));

        final String defaultEntry = properties.getProperty("serial.system.port");
        for (final String entry : portList.getItems()) {
            if (entry.equals(defaultEntry)) {
                portList.getSelectionModel().select(defaultEntry);
                break;
            }
        }

    }

    public void setStage(final Stage stage) {
        assocStage = stage;
    }

    private void populatePortList() {
        portList.setPromptText("Select serial port");

        LOGGER.debug("Sending GetAvailablePortsEvent to Serial Bus");

        serialBus.post(new GetAvailablePortsEvent(x -> {
            if (x.getStatus() == 0) {
                final ObservableList<String> ports = FXCollections.observableArrayList(x.getEntity());
                if (ports.isEmpty()) {
                    disableAll();
                    showWarningDialog();
                } else {
                    portList.setItems(ports);
                    portList.getSelectionModel().select(0);
                }

            } else {
                LOGGER.error(x.getMessage());
                disableAll();
                showWarningDialog();
            }
        }));

    }

    private void showWarningDialog() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Cannot found any available ports");
        alert.setHeaderText("There isn't any available ports.");
        alert.setContentText("Please check your connections and try to reload the application. Thanks!");
        alert.setHeight(300);
        alert.setWidth(800);
        alert.showAndWait();
    }

    private void disableAll() {
        portList.setPromptText("No serial ports detected");
        portList.setDisable(true);

        connectButton.setDisable(true);
        testButton.setDisable(true);
        baudRate.setDisable(true);
    }


    private void showTestingDialog(final Window owner) {
        Platform.runLater(new Task<Void>() {

            @Override
            protected Void call() throws Exception {
                Stage stage = new Stage();
                stage.setTitle("Testing connection...");

                final FXMLLoader loader = new FXMLLoader();

                final TestingConnectionController ctrl = injector.getInstance(TestingConnectionController.class);
                final int baudios = Integer.valueOf(baudRate.getText());
                final String portName = portList.getSelectionModel().getSelectedItem();
                ctrl.setConnection(portName, baudios);
                loader.setControllerFactory(param -> ctrl);

                try (final InputStream stream = getClass().getResourceAsStream(TESTING_FXML)) {
                    final Parent rootNode = loader.load(stream);
                    stage.initStyle(StageStyle.UTILITY);
                    stage.initOwner(owner);
                    stage.initModality(Modality.APPLICATION_MODAL);
                    stage.setResizable(false);
                    stage.setScene(new Scene(rootNode));
                    stage.setOnCloseRequest(event -> stage.close());
                    stage.showAndWait();
                } catch (final IOException ex) {
                    LOGGER.error("Cannot load " + TESTING_FXML + ".", ex);
                }

                return null;
            }
        });
    }

    private void loadMainView() {


        assocStage.hide();
        Stage stage = new Stage();
        stage.setTitle("IESmet 2.0");

        final FXMLLoader loader = new FXMLLoader();
        loader.setControllerFactory(param -> injector.getInstance(MainController.class));


        try (final InputStream stream = getClass().getResourceAsStream(MAIN_FXML)) {
            final Parent rootNode = loader.load(stream);
            stage.initStyle(StageStyle.DECORATED);
            stage.setResizable(true);
            stage.setScene(new Scene(rootNode));
            stage.setOnCloseRequest(event -> {
                stage.close();
                controllerBus.post(new StopAllModulesEvent());
            });
            stage.show();


        } catch (final IOException ex) {
            LOGGER.error("Cannot load " + MAIN_FXML + ".", ex);
        }

    }


    @FXML
    private void testConnectionButtonAction() {
        final Window owner = testButton.getScene().getWindow();
        showTestingDialog(owner);
    }

    @FXML
    private void connectButtonAction() {
        final int baudios = Integer.valueOf(baudRate.getText());
        final String portName = portList.getSelectionModel().getSelectedItem();


        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Connecting...");
        alert.setHeaderText("Connection is in progress. Please wait a few seconds...");
        alert.setHeight(300);
        alert.setWidth(400);
        alert.show();


        //testButton.getScene().getWindow().hide();
        //loadMainView();
        //alert.close();

        serialBus.post(new EstablishConnectionEvent(portName, baudios, 10000, x -> {
            if (x.getStatus() == 0) {
                LOGGER.debug("Loading main view...");
                Platform.runLater(new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
                        loadMainView();
                        alert.close();
                        controllerBus.post(new BeginSchedulerModuleEvent());

                        return null;
                    }
                });
            }
        }));
    }

    @FXML
    private void closeButtonAction() {
        // Get a handle to the stage
        Stage stage = (Stage) closeButton.getScene().getWindow();

        // Do what you have to do
        stage.close();

        controllerBus.post(new StopAllModulesEvent());
    }

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        populatePortList();
        setDefaults();
    }
}
