package es.ies.iesmet.controller;

import com.google.common.eventbus.EventBus;

/**
 * Created by jtorregrosa on 23/03/15.
 */
public abstract class Controller {
    protected EventBus eventBus;

    public Controller(final EventBus eventBus) {
        this.eventBus = eventBus;
    }
}
