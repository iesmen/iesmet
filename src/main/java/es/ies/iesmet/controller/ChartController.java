package es.ies.iesmet.controller;

import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import es.ies.iesmet.injection.annotation.SerialBus;
import es.ies.iesmet.serial.entity.Sensor;
import es.ies.iesmet.serial.entity.SensorImpl;
import es.ies.iesmet.serial.entity.SerialPacket;
import es.ies.iesmet.serial.event.EventBusWrapper;
import es.ies.iesmet.serial.event.SerialPacketEvent;
import es.ies.iesmet.serial.interpretation.DummyInterpreter;
import javafx.animation.AnimationTimer;
import javafx.animation.Timeline;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.util.StringConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;

/**
 * Created by jtorregrosa on 22/03/15.
 */
public class ChartController implements Initializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChartController.class);

    @FXML
    private AnchorPane chartWrapper;

    @FXML
    private AreaChart chart;

    private static final int MAX_DATA_POINTS = 50;
    private XYChart.Series series;
    private int xSeriesData = 0;
    private ConcurrentLinkedQueue<Number> dataQ;

    private long updateMillis;



    private String name;

    public ChartController() {
    }

    public void setDataSeries(final ConcurrentLinkedQueue<Number> series){
        this.dataQ = series;
    }

    public void setName(final String name){
        this.name = name;
    }

    public void setUpdateTime(final long millis){
        this.updateMillis = millis;
    }

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        //-- Chart Series
        chart.setAnimated(false);
        series = new AreaChart.Series<Number, Number>();
        series.setName(name);
        chart.getData().add(series);

        chart.setId("liveAreaChart");
        chart.setTitle(name);
        ((NumberAxis) chart.getXAxis()).setTickLabelFormatter(new StringConverter<Number>() {
            private SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

            @Override
            public String toString(final Number object) {
                final Date date = new Date(object.longValue());
                return sdf.format(date);
            }

            @Override
            public Number fromString(String string) {
                return null;
            }
        });

        prepareTimeline();
    }

    //-- Timeline gets called in the JavaFX Main thread
    private void prepareTimeline() {
        // Every frame to take any data from queue and add to chart
        new AnimationTimer() {
            @Override
            public void handle(long now) {
                addDataToSeries();
            }
        }.start();
    }

    private void addDataToSeries() {
        for (int i = 0; i < 20; i++) { //-- add 20 numbers to the plot+
            if (dataQ.isEmpty()) break;
            final AreaChart.Data data = new AreaChart.Data(new Date().getTime(), dataQ.remove());
            data.setNode(new HoveredThresholdNode(
                    (Double)data.getYValue()
            ));

            series.getData().add(data);
        }
        // remove points to keep us at no more than MAX_DATA_POINTS
        if (series.getData().size() > MAX_DATA_POINTS) {
            series.getData().remove(0, series.getData().size() - MAX_DATA_POINTS);
        }

        // update
        ((NumberAxis) chart.getXAxis()).setLowerBound(new Date().getTime() - MAX_DATA_POINTS * updateMillis + updateMillis);
        ((NumberAxis) chart.getXAxis()).setUpperBound(new Date().getTime() - updateMillis);
    }

    class HoveredThresholdNode extends StackPane {
        HoveredThresholdNode(double value) {
            setPrefSize(10, 10);

            final Label label = createDataThresholdLabel(value);

            setOnMouseEntered(mouseEvent -> {
                getChildren().setAll(label);
                setCursor(Cursor.HAND);
                toFront();
            });
            setOnMouseExited(mouseEvent -> {
                getChildren().clear();
                setCursor(Cursor.CROSSHAIR);
            });


        }

        private Label createDataThresholdLabel(double value) {
            final Label label = new Label(value + "");
            label.getStyleClass().addAll("default-color0", "chart-line-symbol", "chart-series-line");
            label.setStyle("-fx-font-size: 15; -fx-font-weight: bold;");
            label.setTextFill(Color.DARKGRAY);
            //label.setTranslateY(-50);
            label.setMouseTransparent(true);


            label.setMinSize(Label.USE_PREF_SIZE, Label.USE_PREF_SIZE);
            return label;
        }
    }
}
