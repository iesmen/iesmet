package es.ies.iesmet.controller;

import com.google.inject.Inject;
import es.ies.iesmet.event.StopAllModulesEvent;
import es.ies.iesmet.injection.annotation.ControllerBus;
import es.ies.iesmet.injection.annotation.SerialBus;
import es.ies.iesmet.serial.entity.Handshake;
import es.ies.iesmet.serial.event.CloseConnectionEvent;
import es.ies.iesmet.serial.event.EstablishConnectionEvent;
import es.ies.iesmet.serial.event.EventBusWrapper;
import es.ies.iesmet.serial.event.HandshakeRequestCallbackEvent;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * Created by jtorregrosa on 22/03/15.
 */
public class TestingConnectionController implements Initializable {

    /**
     * Internal class logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(TestingConnectionController.class);

    @FXML
    private Label baudRate;
    @FXML
    private Label libVersion;
    @FXML
    private Label boardName;
    @FXML
    private Label upTime;
    @FXML
    private Label statusLabel;

    @FXML
    private Button closeButton;

    private String baudRateStr;
    private String libVersionStr;
    private String boardNameStr;
    private String upTimeStr;

    private String connPortName;
    private int connBaudRate;


    private boolean isDetected;

    @FXML
    private ProgressBar progressBar;

    @Inject
    @ControllerBus
    private EventBusWrapper controllerBus;

    @Inject
    @SerialBus
    private EventBusWrapper serialBus;

    @Inject
    private Properties properties;


    public TestingConnectionController() {
    }

    public void setConnection(final String portName, final int baudRate) {
        this.connPortName = portName;
        this.connBaudRate = baudRate;
    }

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        baudRateStr = "Not detected";
        libVersionStr = "Not detected";
        boardNameStr = "Not detected";
        upTimeStr = "Not supported right now";

        isDetected = false;
        establishConnection();
    }

    private void establishConnection() {
        statusLabel.setText("Establishing connection and waiting 3 second to make it stable...");
        LOGGER.debug("Sending EstablishConnectionEvent to Serial Bus");

        progressBar.setProgress(-1.0d);
        serialBus.post(new EstablishConnectionEvent(connPortName, connBaudRate, 10000, x -> {
            final Task task = new Task<Void>() {
                @Override
                public Void call() {

                    if (x != null && x.getStatus() == 0) {
                        statusLabel.setText("Performing test...");
                        progressBar.setProgress(.5d);
                        runConnectionTest();

                    } else {
                        progressBar.progressProperty().unbind();
                        progressBar.setProgress(1.0d);
                        final Alert alert = new Alert(Alert.AlertType.WARNING);
                        alert.initOwner(progressBar.getScene().getWindow());
                        alert.initModality(Modality.WINDOW_MODAL);
                        alert.setTitle("IESmet board not found");
                        alert.setHeaderText(null);
                        alert.setContentText(
                                "Cannot connect with a board on port '" + connPortName +
                                        "' with '" + connBaudRate + "'baud rate."
                        );
                        alert.showAndWait();

                        boardName.setText("No data");
                        libVersion.setText("No data");
                        baudRate.setText("No data");
                        upTime.setText("No data");

                        statusLabel.setText("Test failed.");
                        LOGGER.error("There is a problem trying to establish serial connection.");
                    }

                    return null;
                }
            };

            Platform.runLater(task);
        }));
    }


    private void runConnectionTest() {
        LOGGER.debug("Sending HandshakeRequestCallbackEvent(10000) to Serial Bus");
        serialBus.post(new HandshakeRequestCallbackEvent(10000, x -> {
            LOGGER.debug("HandshakeCallback called");
            if (x != null && x.getStatus() == 0) {
                final Task task = new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
                        progressBar.progressProperty().bind(this.progressProperty());
                        statusLabel.setText("Test done successfully.");

                        fillHandshakeInfo(x.getEntity());
                        boardName.setText(boardNameStr);
                        libVersion.setText(libVersionStr);
                        baudRate.setText(baudRateStr);
                        upTime.setText(upTimeStr);
                        updateProgress(1, 1);
                        return null;
                    }
                };


                Platform.runLater(task);
            } else {
                final Task task = new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
                        progressBar.progressProperty().bind(this.progressProperty());


                        statusLabel.setText("Test failed.");
                        updateProgress(1, 1);
                        boardName.setText("No data");
                        libVersion.setText("No data");
                        baudRate.setText("No data");
                        upTime.setText("No data");

                        return null;
                    }
                };


                Platform.runLater(task);
            }
            serialBus.post(new CloseConnectionEvent(null));
        }));

    }

    private void fillHandshakeInfo(final Handshake handshake) {
        LOGGER.info("Received a valid handshake from " + handshake.getBoardName());
        boardNameStr = handshake.getBoardName();
        libVersionStr = handshake.getLibraryVersion();
        baudRateStr = String.valueOf(handshake.getBaudRate());
        isDetected = true;
    }

    @FXML
    private void closeButtonAction() {
        final Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();

    }

}
