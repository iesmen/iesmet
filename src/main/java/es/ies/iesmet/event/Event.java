package es.ies.iesmet.event;

/**
 * Created by jtorregrosa on 24/04/15.
 */
public abstract class Event<T> {
    private T response;

    public Event() {
        this(null);
    }

    public Event(final T response) {
        this.response = response;
    }

    public T get() {
        return response;
    }

    public void set(final T entity) {
        this.response = entity;
    }
}
