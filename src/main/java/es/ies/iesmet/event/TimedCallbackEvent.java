package es.ies.iesmet.event;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

/**
 * Created by jtorregrosa on 24/04/15.
 */
public abstract class TimedCallbackEvent<T> {

    private final Consumer<T> callback;
    private T response = null;
    private ScheduledExecutorService scheduledExecutorService;

    public TimedCallbackEvent(final long timeoutMillis, final Consumer<T> callback) {
        this.callback = callback;

        this.scheduledExecutorService = Executors.newScheduledThreadPool(1);

        scheduledExecutorService.schedule(() -> callback.accept(null), timeoutMillis, TimeUnit.MILLISECONDS);
    }

    public void set(final T response) {
        this.response = response;
        if (callback != null) {
            callback.accept(response);
            scheduledExecutorService.shutdownNow();
        }
    }

}
