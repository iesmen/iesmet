package es.ies.iesmet.event;

/**
 * Created by jtorregrosa on 24/04/15.
 */
public class StatusResponse<T> {

    private int status;
    private String message;
    private T entity;

    public StatusResponse() {
        this(-1, null, null);
    }

    public StatusResponse(int status, String message, T entity) {
        this.status = status;
        this.message = message;
        this.entity = entity;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setEntity(T entity) {
        this.entity = entity;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public T getEntity() {
        return entity;
    }
}
