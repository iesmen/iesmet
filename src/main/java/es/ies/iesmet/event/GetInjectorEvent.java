package es.ies.iesmet.event;

import com.google.inject.Injector;

/**
 * Created by jtorregrosa on 26/04/15.
 */
public class GetInjectorEvent extends FutureEvent<Injector> {
}
