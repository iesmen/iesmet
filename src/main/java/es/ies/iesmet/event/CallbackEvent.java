package es.ies.iesmet.event;

import java.util.function.Consumer;

/**
 * Created by jtorregrosa on 24/04/15.
 */
public abstract class CallbackEvent<T> {

    private final Consumer<T> callback;
    private T response = null;

    public CallbackEvent(final Consumer<T> callback) {
        this.callback = callback;
    }

    public void set(final T response) {
        this.response = response;
        if (callback != null) {
            callback.accept(response);
        }
    }

}
