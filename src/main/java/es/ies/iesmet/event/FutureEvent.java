package es.ies.iesmet.event;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Created by jtorregrosa on 24/04/15.
 */
public abstract class FutureEvent<T> {
    private T response = null;
    private CountDownLatch latch = new CountDownLatch(1);

    public T get(long timeoutMS) {
        try {
            latch.await(timeoutMS, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response;
    }

    public void set(T response) {
        this.response = response;
        latch.countDown();
    }

}
