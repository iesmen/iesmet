package es.ies.iesmet.DAO;

import com.mongodb.*;
import es.ies.iesmet.model.Sensor;
import es.ies.iesmet.model.SensorType;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alberto on 19/04/15.
 */
public class SensorTypeDAO extends EntityDAO<SensorType> {
    private static final Logger LOGGER = LoggerFactory.getLogger(SensorTypeDAO.class);
    public SensorTypeDAO() {
        setConnection("sensorType");
    }

    @Override
    public SensorType objectToEntity(DBObject item) {
        ObjectId oId = (ObjectId)item.get("_id");

        String value = (String)item.get("value");
        String description = (String) item.get("description");
        SensorType type = new SensorType(oId.toString(), value, description);
        return type;
    }

    @Override
    public List<SensorType> getAll() {
        DBCursor cursor = collection.find();
        List<SensorType> list = new ArrayList<SensorType>();
        try {
            SensorType sensor;
            while(cursor.hasNext()) {
                DBObject item = cursor.next();
                sensor = objectToEntity(item);
                System.out.println(sensor);
                list.add(sensor);
            }
        } finally {
            cursor.close();
        }
        return list;
    }

    @Override
    public SensorType getById(String id) {
        BasicDBObject query = new BasicDBObject();
        query.put("_id", new ObjectId(id));
        LOGGER.info("Document with id " + id + " requested");
        try {
            DBObject item = collection.findOne(query);
            SensorType sensor = objectToEntity(item);
            LOGGER.info("Document with id " + id + " found successfully");
            return sensor;
        }
        catch (MongoException ex) {
            LOGGER.error("Error getting element with id " + id);
        }
        return null;
    }

    @Override
    public SensorType create(SensorType entity) {
        SensorType type = new SensorType(entity.getValue(),entity.getDescription());

        BasicDBObject object = new BasicDBObject();
        object.append("value", type.getValue());
        object.append("description", type.getDescription());

        try {
            WriteResult result = collection.insert(WriteConcern.SAFE, object);
            type.setId(object.getObjectId("_id").toString());
            LOGGER.info("Document with id " + type.getId() + " created successfully");
            return type;
        }
        catch (MongoException ex) {
            LOGGER.error("Error creating document with id " + type.getId());
        }

        return null;
    }

    @Override
    public void delete(String id) {
        BasicDBObject query = new BasicDBObject();
        query.put("_id", new ObjectId(id));
        LOGGER.info("Document with id " + id + " requested for being deleted");
        try {
            collection.remove(query);
            LOGGER.info("Document with id " + id + " deleted successfully");
        }
        catch (MongoException ex) {
            ex.printStackTrace();
            LOGGER.error("Error deleting document with id " + id);
        }
    }

    @Override
    public SensorType getByValue(String value) {
        BasicDBObject query = new BasicDBObject();
        query.put("value", value);

        try {
            DBObject item = collection.findOne(query);
            SensorType type = objectToEntity(item);
            return type;
        }
        catch (MongoException ex) {
        }

        return null;
    }
}
