package es.ies.iesmet.DAO;

import com.mongodb.*;
import es.ies.iesmet.model.Sensor;
import es.ies.iesmet.model.SensorType;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alberto on 4/04/15.
 */
public class SensorDAO extends EntityDAO<Sensor> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SensorDAO.class);
    public SensorDAO() {
        setConnection("sensor");
    }

    @Override
    public Sensor objectToEntity(DBObject item) {
        ObjectId oId = (ObjectId)item.get("_id");
        double value = (Double)item.get("value");
        Timestamp time = Timestamp.valueOf((String) item.get("timestamp"));
        SensorTypeDAO typeDAO = new SensorTypeDAO();
        SensorType type = typeDAO.getById((String) item.get("type"));

        Sensor sensor = new Sensor(oId.toString(), value, time, type);
        return sensor;
    }

    @Override
    public List<Sensor> getAll() {
        DBCursor cursor = collection.find();
        List<Sensor> list = new ArrayList<Sensor>();
        try {
            Sensor sensor;
            while(cursor.hasNext()) {
                DBObject item = cursor.next();
                sensor = objectToEntity(item);
                System.out.println(sensor);
                list.add(sensor);
            }
        } finally {
            cursor.close();
        }
        return list;
    }

    @Override
    public Sensor getById(String id) {
        BasicDBObject query = new BasicDBObject();
        query.put("_id", new ObjectId(id));
        LOGGER.info("Document with id " + id + " requested");
        try {
            DBObject item = collection.findOne(query);
            Sensor sensor = objectToEntity(item);
            LOGGER.info("Document with id " + id + " found successfully");
            return sensor;
        }
        catch (MongoException ex) {
            LOGGER.error("Error getting element with id " + id);
        }
        return null;
    }

    @Override
    public Sensor getByValue(String value) {
        BasicDBObject query = new BasicDBObject();
        query.put("value", new ObjectId(value));

        try {
            DBObject item = collection.findOne(query);
            Sensor sensor = objectToEntity(item);
            return sensor;
        }
        catch (MongoException ex) {
        }

        return null;
    }

    @Override
    public Sensor create(Sensor entity) {
        Sensor sensor = new Sensor(entity.getValue(),entity.getTimestamp(), entity.getType());

        BasicDBObject object = new BasicDBObject();
        object.append("value", sensor.getValue());
        object.append("timestamp", sensor.getTimestamp().toString());
        object.append("type", sensor.getType().getId());
        System.out.println(object);
        try {
            WriteResult result = collection.insert(WriteConcern.SAFE,object);
            sensor.setId(object.getObjectId("_id").toString());
            LOGGER.info("Document with id " + sensor.getId() + " created successfully");
            return sensor;
        }
        catch (MongoException ex) {
            LOGGER.error("Error creating document with id " + sensor.getId());
        }

        return null;
    }

    @Override
    public void delete(String id) {
        BasicDBObject query = new BasicDBObject();
        query.put("_id", new ObjectId(id));
        LOGGER.info("Document with id " + id + " requested for being deleted");
        try {
            collection.remove(query);
            LOGGER.info("Document with id " + id + " deleted successfully");
        }
        catch (MongoException ex) {
            ex.printStackTrace();
            LOGGER.error("Error deleting document with id " + id);
        }
    }
}
