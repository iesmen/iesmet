package es.ies.iesmet.DAO;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import java.util.List;
/**
 * @param <T> Entity Type
 * @author Alberto Real [albertorealfdez@gmail.com]
 * @author Jorge Torregrosa [jtorregrosalloret@gmail.com]
 * @version 1.0.1
 *
 */
public abstract class EntityDAO<T> {
    private static String server = "localhost";
    private static int serverNum = 27017;
    private static String dbName = "iesdb";
    private MongoClient client;
    private DB db;
    protected DBCollection collection;

    public void setConnection(String collectionName) {
        client = new MongoClient( server , serverNum );
        db =  client.getDB(dbName);
        collection = db.getCollection(collectionName);
    }

    /**
     * Convert a database object to a concrete entity
     * @param item To transform
     * @return Converted entity
     */
    public abstract T objectToEntity(DBObject item);

    /**
     * Get all documents (registers) in a concrete collection (table)
     * @return The list with the documents as the concrete entity
     */
    public abstract List<T> getAll();

    /**
     * Get a concrete entity  with the especified id
     * @param id An entity id
     * @return The founded entity
     */
    public abstract T getById(String id);

    /**
     * Get one or more entities  with the especified value
     * @param value A value to find
     * @return The founded entity
     */
    public abstract T getByValue(String value);

    /**
     * Create a new registry
     * @param entity Entity to create
     * @return The created entity
     */
    public abstract T create(T entity);

    /**
     * Delete a given registry
     * @param id Id of the entity to delete
     * @return
     */
    public abstract void delete(String id);
}
