package es.ies.iesmet.injection;

import com.google.inject.AbstractModule;

import java.util.Properties;

/**
 * Created by jtorregrosa on 23/03/15.
 */
public class PropertiesModule extends AbstractModule {

    private final Properties props;


    public PropertiesModule(final Properties props) {
        this.props = props;
    }

    @Override
    protected void configure() {
        bind(Properties.class).toInstance(props);
    }
}
