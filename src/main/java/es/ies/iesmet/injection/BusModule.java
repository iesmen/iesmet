package es.ies.iesmet.injection;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.google.inject.matcher.Matchers;
import com.google.inject.spi.InjectionListener;
import com.google.inject.spi.TypeEncounter;
import com.google.inject.spi.TypeListener;
import es.ies.iesmet.injection.annotation.ControllerBus;
import es.ies.iesmet.injection.annotation.SerialBus;
import es.ies.iesmet.serial.event.EventBusWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by jtorregrosa on 23/03/15.
 */
public class BusModule extends AbstractModule {

    private final Logger LOGGER = LoggerFactory.getLogger(BusModule.class);
    private final EventBusWrapper serialBus;
    private final EventBusWrapper controllerBus;


    public BusModule(final EventBusWrapper serialBus, final EventBusWrapper controllerBus) {
        this.serialBus = serialBus;
        this.controllerBus = controllerBus;
    }

    @Override
    protected void configure() {
        bind(EventBusWrapper.class).annotatedWith(SerialBus.class).toInstance(serialBus);
        bind(EventBusWrapper.class).annotatedWith(ControllerBus.class).toInstance(controllerBus);
        bindListener(Matchers.any(), new TypeListener() {
            @Override
            public <I> void hear(@SuppressWarnings("unused") final TypeLiteral<I> typeLiteral, final TypeEncounter<I> typeEncounter) {
                typeEncounter.register((InjectionListener<I>) instance -> {
                    LOGGER.debug("Registering buses in {}", instance.getClass().getCanonicalName());
                    serialBus.register(instance);
                    controllerBus.register(instance);
                });
            }
        });
    }
}
