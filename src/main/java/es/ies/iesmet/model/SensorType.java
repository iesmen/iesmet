package es.ies.iesmet.model;

/**
 * Created by alberto on 19/04/15.
 */
public class SensorType {
    public static enum Type {
        TEMPERATURE,
        PRESSURE,
        HUMIDITY,
        WIND_SPEED,
        WIND_DIRECTION,
        RAIN_GAUGE
    }

    private String id;
    private Type value;
    private String description;

    public SensorType(String id, String value, String description) {
        this.id = id;
        this.value = Type.valueOf(value);
        this.description = description;
    }

    public SensorType(Type value, String description) {
        this.value = value;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Type getValue() {
        return value;
    }

    public void setValue(Type value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "SensorType{" +
                "id='" + id + '\'' +
                ", value=" + value +
                ", description='" + description + '\'' +
                '}';
    }
}
