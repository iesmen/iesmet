package es.ies.iesmet.model;

import java.sql.Timestamp;

/**
 * Sensor model class. It holds all user information.
 *
 * @author Alberto Real [albertorealfdez@gmail.com]
 * @author Jorge Torregrosa [jtorregrosalloret@gmail.com]
 * @version 1.0
 */
public class Sensor {
    private String id;
    private Timestamp timestamp;
    private double value;
    private SensorType type;

    public Sensor(String id, double value, Timestamp timestamp, SensorType type) {
        this.id = id;
        this.value = value;
        this.timestamp = timestamp;
        this.type = type;
    }

    public Sensor(double value, Timestamp timestamp, SensorType type) {
        this.value = value;
        this.timestamp = timestamp;
        this.type = type;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public Sensor(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public SensorType getType() { return type; }

    public void setType(SensorType type) { this.type = type; }

    public String getTypeValue() {
        return String.valueOf(type.getValue());
    }

    @Override
    public String toString() {
        return "Sensor{" +
                "id=" + id +
                ", value=" + value +
                ", timestamp=" + timestamp +
                ", type=" + type.getValue();
    }
}
