package es.ies.iesmet;

import com.google.inject.Guice;
import com.google.inject.Injector;
import es.ies.iesmet.controller.BootstrapController;
import es.ies.iesmet.injection.BusModule;
import es.ies.iesmet.injection.PropertiesModule;
import es.ies.iesmet.module.*;
import es.ies.iesmet.serial.SerialManager;
import es.ies.iesmet.serial.event.EventBusWrapper;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


/**
 * Test Comment. This is a test comment.
 *
 * @author Author Name
 * @version 1.0
 * @see javafx.application.Application
 */
public class MainApp{

    /**
     * Internal class logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(MainApp.class);


    /**
     * Application serial pool.
     */
    private SerialManager pool;


    /**
     * IESmet properties file.
     */
    private static final String PROPERTIES_FILE = "/config.properties";


    /**
     * Program entry point.
     *
     * @param args program arguments.
     * @throws Exception if something goes wrong.
     */
    public static void main(final String[] args) throws Exception {
        //LOGGER.debug("Loading IESmet properties file: {}", PROPERTIES_FILE);

       //launch(args);
        // Load configuration properties
        final Properties prop = new Properties();
        prop.load(MainApp.class.getResourceAsStream(PROPERTIES_FILE));

        final EventBusWrapper serialBus = new EventBusWrapper("serialbus");
        final EventBusWrapper controllerBus = new EventBusWrapper("controllerbus");

        final BusModule busModule = new BusModule(
                serialBus,
                controllerBus
        );

        final PropertiesModule propsModule = new PropertiesModule(
                prop
        );

        final Injector ctrInjector = Guice.createInjector(busModule, propsModule);
        final SerialModule serialModule = ctrInjector.getInstance(SerialModule.class);
        final ViewModule viewModule = ctrInjector.getInstance(ViewModule.class);
        //final PersistenceModule persModule = ctrInjector.getInstance(PersistenceModule.class);
        final RequestSchedulerModule requestModule = ctrInjector.getInstance(RequestSchedulerModule.class);


        final ModuleManager moduleMgr = ctrInjector.getInstance(ModuleManager.class);

        moduleMgr.register(serialModule);
        moduleMgr.register(viewModule);
        //moduleMgr.register(persModule);
        moduleMgr.register(requestModule);
        moduleMgr.startAll();

        // Explicit exit due to JAVAFX running threads
        System.exit(0);
    }


}
